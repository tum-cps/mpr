# Model Predictive Robustness

![image info](./docs/figures/mpr-banner.png)

![stl-robustness](https://img.shields.io/badge/stl-robustness-red?style=plastic&logo=Trustpilot)
![test-passed](https://img.shields.io/badge/tests-passed-yellow?style=plastic&logo=AngelList)

The robustness of signal temporal logic not only evaluates whether a signal complies with a specification but 
also provides a measure of how much a formula is fulfilled or violated. 
The robustness of predicates is an essential building block for evaluating the robustness of the entire formula. 
However, the robustness is typically defined in a model-free way, that is, without considering the underlying 
system dynamics. To solve this problem, we propose the **model predictive robustness** (MP Robustness).
This is the repository providing the interface for:
1. computing MP robustness; 
2. generating ground truth data from a dataset; and
3. training and applying nonlinear regression models.

The structure of the repository is:
```
├── commonroad_mpr
│   ├── common
│      ├── vehicle.py                      # vehicle models, states and trajectories
│      ├── road_network.py                 # lane structures
│      ├── observation.py                  # "world state", observation based on the vehicles and the environments
│      └── predicates.py                   # traffic rule predicate definition and evaluation (Boolean, aka characteristic function)
│   ├── prediction
│      ├── ego_sampling.py                 # polynomial-based sampling approach
│      └── other_prediction.py             # recorded behaviors from data-set
│   ├── learning
│      ├── feature_variable.py             # feature variables
│      ├── data_generation.py              # generation of input-output pairs
│      ├── data_loader.py                  # load generated date
│      └── gp_regression.py                # training and evaluation of the model
│   ├── utils
│      ├── visualization.py                # feature variables
│      └── configuration_builder.py        # builder of the configuration
│   └── mpr.py                             # MPR class, a wrapper for robustness evaluation
├── config_files                           # default and user-defined configurations
├── docs                                   # documenting the code
├── scenarios                              # exemplary scenarios
├── tests                                  # unit tests
├── tutorials                              # tutorials for robustness evaluation and learning
├── LICENSE                                # GNU GENERAL PUBLIC LICENSE
├── setup.py                               # installation and version control
└── README                                 # Readme file 
```
## Table of Contents

- [Install](#install) <!-- - [Usage](#usage) -->
- [Running the code](#Running-the-code)
- [Reference](#Reference)
- [Contact Us](#contact-us)
<!-- - [Literature](#literature) -->
<!-- - [License](#license) -->

<!-- ## Background

MB robustness is defined based on the dynamic model of the vehicles. It is sound, generic, and scalable.

### Any optional sections -->

## Install

We recommend using [anaconda](https://www.anaconda.com/) to manage the python environments. We start by creating a new conda environment:

```bash
$ conda create -n commonroad-py39 python=3.9
$ conda activate commonroad-py39
```

Install pytorch and the Jupyter kernel
```bash
$ conda install pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch
$ conda install -n python_env ipykernel
```

Then install the package to development path:

```bash
$ cd <path-to-this-repo>
$ pip install .
$ conda develop .
```

To test the installition, run unittest:
```bash
$ cd tests
$ python -m unittest -v
```

To get started your journey with model predictive robustness, check the `tutorials`.

<!-- ## Usage

```
```

### Any optional sections

## API

### Any optional sections -->

## Running the code
To run the tutorials, you need to know:

**Scenarios**:
We highly recommend you to first try out CommonRoad scenarios. For learning purposes, we provide a converter to convert the highD dataset into CommonRoad scenarios in `.xml` format. 
The public converter is available [here](https://commonroad.in.tum.de/tools/dataset-converters). 
For instance, we use this command to convert highD scenarios (internally, you can install the source file from [this link](https://nextcloud.in.tum.de/index.php/s/78mjHBR4om4iyb9)):
```bash
$ crconvert highd <source folder>  <target folder>  --num-processes 4 --keep-ego
```

**Output and Configuration**:
The outputs will be stored in the ``./output/`` folder.
Default and scenario-specific configurations are stored in the ``./config_files/`` folder.

## Reference

If you find our robustness definition, code, or paper useful for your research,
please include the following citation:

```angular2html
@article{mpr2023,
	author = {Lin, Yuanfei and  Li, Haoxuan and  Althoff, Matthias},
	title = {Model Predictive Robustness of Signal Temporal Logic Predicates},
	journal = {IEEE Robotics and Automation Letters},
	year = {2023},
	volume = {08},
	month = {10},
	number = {12},
	pages = {8050 - 8057},
	doi = {10.1109/LRA.2023.3324582},
}
```
(for more details, pls check https://doi.org/10.1109/LRA.2023.3324582)

## Contact Us
If you have questions regarding the dataset or code, please email us at yuanfei.lin@tum.de or haoxuan.li@tum.de. 
We will get back to you as soon as possible.
<!-- ## Literature -->

<!-- ## License

[MIT © Richard McRichface.](../LICENSE) -->
