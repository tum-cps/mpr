#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='commonroad-mpr',
      version='0.0.1',
      description='Model predictive robustness of signal temporal logic predicates',
      keywords="robustness, STL",
      author='Haoxuan Li, Yuanfei Lin',
      author_email='haoxuan.li@tum.de, yuanfei.lin@tum.de',
      url='https://gitlab.lrz.de/tum-cps/mpr',
      license='GPLv3',
      packages=find_packages(),
      install_requires=[
          'commonroad-io>=2022.3',
          'commonroad-drivability-checker>=2022.2',
          'matplotlib>=3.5.2',
          'numpy>=1.19.0',
          'pandas>=1.4.2',
          'omegaconf>=2.1.1',
          'scipy>=1.7.3',
          'gpytorch>=1.8.0',
      ],
      extras_require={
          'tests': [
              'pytest>=7.1'
          ]
      })
