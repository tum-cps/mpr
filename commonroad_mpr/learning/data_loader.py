from multiprocessing.sharedctypes import Value
import os
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg

from .feature_variable import FeatureExtrator

if TYPE_CHECKING:
    from typing import List, Optional, Tuple, Dict


def project(x: "np.ndarray", min: float, max: float) -> "np.ndarray":
    if len(x) <= 1:
        # CAUTION: even an empty list was given, will return [max].
        return [max]
    x_min = np.min(x)
    x_max = np.max(x)
    if x_min == x_max:
        return [max] * len(x)
    return (x - x_min) / (x_max - x_min) * (max - min) + min


def normalize(robustness: "np.ndarray") -> "np.ndarray":
    mask = robustness > 0
    normalized_robustness = np.array(robustness)
    normalized_robustness[mask] = project(normalized_robustness[mask], 0, 1)
    normalized_robustness[~mask] = project(normalized_robustness[~mask], -1, 0)
    return normalized_robustness


class DataLoader:
    @classmethod
    def create_from_config(
        cls,
        predicate_names: "List[str]" = None,
        load_filename: "Optional[str]" = None,
        save_filename: "Optional[str]" = None,
        load_filepath: "Optional[str]" = None,
        save_filepath: "Optional[str]" = None,
    ) -> "DataLoader":
        return cls(
            **Cfg["learning_data"]["data_loader"],
            predicate_names=predicate_names,
            reference_names=Cfg["robustness"]["output_names"],
            load_filename=load_filename,
            save_filename=save_filename,
            load_filepath=load_filepath,
            save_filepath=save_filepath
        )

    def __init__(
        self,
        predicate_names: "List[str]",
        entry_id_names: "List[str]",
        reference_names: "List[str]",
        output_names: "List[str]",
        load_filename: "Optional[str]" = None,
        save_filename: "Optional[str]" = None,
        load_filepath: "Optional[str]" = None,
        save_filepath: "Optional[str]" = None,
    ) -> None:
        self.predicate_names = predicate_names
        self.entry_id_names = entry_id_names
        self.reference_names = reference_names
        self.output_names = output_names

        self.load_filename = load_filename
        self.save_filename = save_filename
        self.load_filepath = load_filepath
        self.save_filepath = save_filepath
        if self.load_filename is not None or self.load_filepath is not None:
            self.load_data()
        else:
            self.generate_empty_dataframe()

    @property
    def predicate_names(self) -> "List[str]":
        if not hasattr(self, "_predicate_names") or self._predicate_names is None:
            self._predicate_names = Cfg["learning_data"]["data_generator"][
                "predicate_names"
            ]
        return self._predicate_names

    @predicate_names.setter
    def predicate_names(self, value) -> None:
        self._predicate_names = value

    @property
    def load_path(self):
        if self.load_filepath is not None:
            return self.load_filepath
        elif self.load_filename is not None:
            return os.path.join(Cfg["path"]["path_learning_data"], self.load_filename)
        else:
            return None

    @property
    def save_path(self):
        if self.save_filepath is not None:
            return self.save_filepath
        elif self.save_filename is not None:
            return os.path.join(Cfg["path"]["path_learning_data"], self.save_filename)
        else:
            return None

    def Xy(self, predicate_name: str, flag_info: bool = False) -> "Option[Tuple[np.ndarray, np.ndarray], \
                                                                  Tuple[np.ndarray, np.ndarray,np.ndarray]]":
        """get the input X and output y of the predicate for training and testing regression models.

        Args:
            predicate_name (str): name of the predicate
            flag_info (bool): information for evaluating the predicates

        Returns:
            X : inputs
            y : outputs
            Z : information
        """
        desired_features = FeatureExtrator.get_desired_features(predicate_name)
        index_features = [
            ("inputs", key, value)
            for key, inner_list in desired_features.items()
            if inner_list is not None
            for value in inner_list
        ]
        desired_predicate_evaluation = FeatureExtrator.get_desired_predicate_evaluation(
            predicate_name
        )
        index_features += [
            ("predicates", predicate_name, value)
            for value in desired_predicate_evaluation
        ]
        arity = FeatureExtrator.get_arity(predicate_name)
        if arity == 1:
            # reset index
            data = self.data.reset_index(self.entry_id_names[-1])
            # remove duplicates
            data = data[~data.index.duplicated(keep="first")]
        else:
            data = self.data
        # drop rows with nan
        data = data.dropna()
        X = data.loc[:, index_features]
        y = data.predicates[predicate_name][self.output_names[0]]
        if flag_info:
            return X.values, y.values, y.index.values
        else:
            return X.values, y.values

    def generate_empty_dataframe(self):
        index = pd.MultiIndex.from_tuples([], names=self.entry_id_names)
        columns = None
        for (
            vehicle,
            list_feature_variable_names,
        ) in FeatureExtrator.all_feature_variable_names.items():
            if columns is None:
                columns = pd.MultiIndex.from_product(
                    [["inputs"], [vehicle], list_feature_variable_names]
                )
            else:
                columns = columns.append(
                    pd.MultiIndex.from_product(
                        [["inputs"], [vehicle], list_feature_variable_names]
                    )
                )
        columns = columns.append(
            pd.MultiIndex.from_product(
                [
                    ["predicates"],
                    self.predicate_names,
                    self.reference_names + self.output_names,
                ]
            )
        )
        self.data = pd.DataFrame(index=index, columns=columns)

    def load_data(self, file_name: "Optional[str]" = None):
        if file_name is not None:
            self.load_filename = file_name
        self.data = pd.read_csv(
            self.load_path,
            header=[0, 1, 2],
            index_col=list(range(len(self.entry_id_names))),
        )
        self.predicate_names = (
            self.data.predicates.columns.get_level_values(0).unique().to_list()
        )

    def save_data(self, file_name: "Optional[str]" = None):
        if file_name is not None:
            self.save_filename = file_name
        self.generate_normalize_robustness()
        self.data.to_csv(self.save_path)

    def append(self, *args, **kwargs) -> None:
        """append data to dataset
        Args:
            dict_entry_id (Dict):
            dict_feature (Dict):
            dict_predicates (Dict):
        Or Args:
            data (pd.DataFrame):
        """
        if len(args) == 1:
            self.append_dataframe(*args)
        elif len(args) == 3:
            self.append_result(*args)
        elif len(kwargs) == 1:
            self.append_dataframe(**kwargs)
        elif len(kwargs) == 3:
            self.append_result(**kwargs)
        else:
            raise ValueError(
                "The input is not valid. Expected are three dictionaries or one dataframe."
            )

    def append_dataframe(self, data: "pd.DataFrame"):
        self.data = pd.concat([self.data, data])

    def append_result(
        self, dict_entry_id: "Dict", dict_feature: "Dict", dict_predicates: "Dict"
    ):
        entry_id = tuple(dict_entry_id[f] for f in self.entry_id_names)
        self.data.loc[entry_id, :] = None
        self.data.loc[entry_id, "inputs"].update(self.flatten_dict(dict_feature))
        self.data.loc[entry_id, "predicates"].update(self.flatten_dict(dict_predicates))

    @staticmethod
    def flatten_dict(nested_dict: "Dict"):
        return {
            (outerKey, innerKey): values
            for outerKey, innerDict in nested_dict.items()
            for innerKey, values in innerDict.items()
        }

    def generate_normalize_robustness(self) -> None:
        for predicate_name in self.predicate_names:
            robustness = self.data.predicates.loc[
                :, (predicate_name, self.reference_names[-1])
            ].values
            self.data.loc[
                :, ("predicates", predicate_name, self.output_names[0])
            ] = normalize(robustness)

