import itertools
import logging
import os
import time
from multiprocessing import Process, Queue, Semaphore
from typing import TYPE_CHECKING

import numpy as np
from commonroad_mpr.common import ScenarioReader
from commonroad_mpr.mpr import ModelPredictiveRobustness as MPR
from commonroad_mpr.prediction import StateBasedSampling
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg

from .data_loader import DataLoader
from .feature_variable import FeatureExtrator

if TYPE_CHECKING:
    import threading
    from typing import Iterable, List, Optional

    from commonroad_mpr.common import World


class DataGenerator:
    @classmethod
    def create_from_config(
        cls,
        load_filename: "Optional[str]" = None,
        save_filename: "Optional[str]" = None,
        load_filepath: "Optional[str]" = None,
        save_filepath: "Optional[str]" = None,
    ) -> "DataLoader":
        """create data generator using config files

        Args:
            load_filename (Optional[str], optional): The already generated dataset in output dir, if given, the new generated data will append to it. Defaults to None.
            save_filename (Optional[str], optional): The save filename of the dataset in output dir. Defaults to None.
            load_filepath (Optional[str], optional): The already generated dataset in absolute or relative path, if given, the new generated data will append to it. Defaults to None.
            save_filepath (Optional[str], optional): The save filename of the dataset in absolute or relative path. Defaults to None.

        Returns:
            DataLoader:
        """
        return cls(
            **Cfg["learning_data"]["data_generator"],
            load_filename=load_filename,
            save_filename=save_filename,
            load_filepath=load_filepath,
            save_filepath=save_filepath,
        )

    def __init__(
        self,
        predicate_names: "List[str]",
        scenario_duration: int,
        dt: float,
        max_scenario_number: int = 1,
        log_filename: str = "data_generation.log",
        time_step_per_scenario: int = 1,
        vehicle_pair_steps: int = 20,
        load_filename: "Optional[str]" = None,
        save_filename: "Optional[str]" = None,
        load_filepath: "Optional[str]" = None,
        save_filepath: "Optional[str]" = None,
    ):
        self.predicate_names = predicate_names
        self.max_scenario_number = max_scenario_number

        # logging
        self.log_filename = log_filename
        self._init_logger()

        # property
        self.time_step_per_scenario = time_step_per_scenario
        self.vehicle_pair_steps = vehicle_pair_steps

        # create data_loader
        self.data_loader = DataLoader.create_from_config(
            predicate_names=self.predicate_names,
            load_filename=load_filename,
            save_filename=save_filename,
            load_filepath=load_filepath,
            save_filepath=save_filepath,
        )

        # define iterations
        self.scenario_duration = scenario_duration  # const
        self.dt = dt  # const
        self.num_ts = StateBasedSampling.compute_num_ts(
            Cfg["sampling_approach"]["state_based_sampling"]["time_horizon"],
            self.dt,
        )
        self.time_step_iteration = [
            i.item()
            for i in np.linspace(
                0,
                self.scenario_duration - self.num_ts,
                self.time_step_per_scenario,
                dtype=int,
            )
        ]

        # define robustness evaluator
        self.mpr = MPR(self.predicate_names)

    @property
    def log_path(self) -> str:
        return os.path.join(Cfg["path"]["path_logs"], self.log_filename)

    def _init_logger(self) -> None:
        logger = logging.getLogger("data_generator")
        if not getattr(logger, "is_initialized", False):
            logger.setLevel(logging.DEBUG)
            fileHandler = logging.FileHandler(self.log_path)
            fileHandler.setLevel(logging.DEBUG)
            formatter = logging.Formatter(
                "[%(asctime)s] <%(processName)-11s> {%(filename)s:%(lineno)d} %(levelname)s - %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
            )
            fileHandler.setFormatter(formatter)
            logger.addHandler(fileHandler)

            if Cfg["debug"]["debug_on"] and Cfg["debug"]["print_progress"]:
                consoleHandler = logging.StreamHandler()
                consoleHandler.setLevel(logging.DEBUG)
                consoleHandler.setFormatter(formatter)
                logger.addHandler(consoleHandler)

            logger.is_initialized = True

        self.logger = logger

    def generate_data_parallel(self):
        def joined(process: "Process"):
            process.join(1)
            if process.exitcode is not None:
                self.logger.info(f"Process {process.name} joint.")
                return True
            return False

        queue = Queue()
        num_worker = Cfg["general"]["num_threads"]
        semaphore = Semaphore(num_worker)
        list_processes: "List[Process]" = []
        queue.put(self.data_loader.data)
        for world_state in self.world_state_iter():
            semaphore.acquire()
            process = Process(
                target=self.process_world_state_parallel,
                args=(world_state, queue, semaphore),
            )
            list_processes.append(process)
            process.start()

        while semaphore.get_value() < num_worker:
            time.sleep(1)

        # count_joined = 0
        while len(list_processes) > 1:
            list_processes[:] = [
                process for process in list_processes if not joined(process)
            ]

        self.data_loader.data = queue.get()
        self.data_loader.save_data()

        while len(list_processes) > 0:
            list_processes[:] = [
                process for process in list_processes if not joined(process)
            ]

    def generate_data(self):
        for world_state in self.world_state_iter():
            self.process_world_state(world_state)
        self.data_loader.save_data()

    def world_state_iter(self) -> "Iterable[World]":
        i = 0
        for world_state in ScenarioReader.world_state_iter():
            if str(world_state.scenario.scenario_id) in self.data_loader.data.index:
                continue
            if i >= self.max_scenario_number:
                break
            yield world_state
            i += 1

    def process_world_state_parallel(
        self,
        world_state: "World",
        queue: "Queue",
        semaphore: "threading.Semaphore",
    ) -> None:
        # clear data at subprocess
        self.data_loader.generate_empty_dataframe()
        for time_step in self.time_step_iteration:
            self.process_time_step(time_step, world_state)
        self.data_loader.append_dataframe(queue.get())
        self.data_loader.save_data()
        queue.put(self.data_loader.data)
        semaphore.release()

    def process_world_state(self, world_state: "World"):
        for time_step in self.time_step_iteration:
            self.process_time_step(time_step, world_state)
        self.data_loader.save_data()

    def vehicle_ids_iter(self, world_state: "World", time_step):
        id_list = []
        for vehicle in world_state.vehicles:
            if vehicle.trajectory_persp.covers_time_range(
                time_step, num_ts=self.num_ts
            ):
                id_list.append(vehicle.id)
        for i, vehicle_ids in enumerate(itertools.combinations(id_list, 2)):
            if i % self.vehicle_pair_steps == 0:
                yield vehicle_ids

    def process_time_step(self, time_step, world_state: "World"):
        for vehicle_ids in self.vehicle_ids_iter(world_state, time_step):
            try:
                self.process_vehicles(vehicle_ids, time_step, world_state)
                self.process_vehicles(
                    list(reversed(vehicle_ids)), time_step, world_state
                )
            except Exception as e:
                if Cfg["debug"]["debug_on"]:
                    raise e
                else:
                    self.logger.exception("")

    def process_vehicles(self, vehicle_ids, time_step, world_state: "World"):
        log_msg = (
            f"PROCESSING {world_state.scenario.scenario_id}:{time_step}:{vehicle_ids}"
        )
        self.logger.info(log_msg)
        dict_predicates = self.mpr.evaluate(
            world_state=world_state, vehicle_ids=vehicle_ids, time_step=time_step
        )
        dict_features = FeatureExtrator.all_feature_variables(
            world_state=world_state, vehicle_ids=vehicle_ids, time_step=time_step
        )
        dict_entry_id = {
            "scenario_id": str(world_state.scenario.scenario_id),
            "time_step": time_step,
            "ego_id": vehicle_ids[0],
            "other_id": vehicle_ids[1],
        }
        self.data_loader.append_result(dict_entry_id, dict_features, dict_predicates)
