import logging
import os
import warnings
from typing import TYPE_CHECKING

import gpytorch
import matplotlib.pyplot as plt
import numpy as np
import torch
from commonroad_mpr.common import PredicateEvaluator, parse
from commonroad_mpr.learning import DataLoader, FeatureExtrator
from commonroad_mpr.learning.data_loader import DataLoader
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

if TYPE_CHECKING:
    from typing import Any, List, Optional, Tuple, Union, Dict

    import numpy as np
    from matplotlib.axes import Axes
    from matplotlib.figure import Figure

_device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class ExactGPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood):
        super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(
            gpytorch.kernels.RBFKernel(ard_num_dims=train_x.shape[1])
        )
        self.X = None
        self.observed_pred = None

    def forward(self, x):
        """
        This function is called during the forward pass of the Gaussian Process (GP) model.
        :param x: input data
        :return: the distribution
        """
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

    def predict(self, X: "np.ndarray") -> "Tuple[float, float]":
        """
        This function is used to make predictions with the GP model.
        :param X: the test data for prediction
        :return: the predicted mean and covariance of the GP
        """
        # sets the GP model and its associated likelihood into evaluation mode
        self.eval()
        self.likelihood.eval()
        # The test data is converted to a PyTorch tensor and moved to the appropriate device
        self.X = torch.autograd.Variable(torch.Tensor(X).to(_device), requires_grad=True)
        warnings.filterwarnings("ignore")
        # with torch.no_grad(), gpytorch.settings.fast_pred_var():
        # The prediction is performed using the trained GP model and likelihood
        self.observed_pred = self.likelihood(self(self.X))
        y = self.observed_pred.mean
        std = self.observed_pred.stddev
        warnings.filterwarnings("default")
        if len(y) == 1:
            # If the length of y (predicted mean) is 1, it means there is a single prediction, so the function returns
            # the predicted mean and standard deviation as separate float values.
            return y.detach().item(), std.detach().item()
        else:
            # Otherwise, if there are multiple predictions, the function returns the predicted mean and standard
            # deviation as numpy arrays, each wrapped in a singleton dimension.
            return (
                y.unsqueeze(1).detach().cpu().numpy(),
                std.unsqueeze(1).detach().cpu().numpy(),
            )

    def get_gradient(self, X: "torch.Tensor"):
        """
        This function computes the gradient w.r.t the input values.
        The computation is based on Berkenkamp, Felix, and Angela P. Schoellig. "Safe and robust learning control with
        Gaussian processes." 2015 European Control Conference (ECC). IEEE, 2015.
        :param X: the test data for gradient computation
        :return: the computed gradient
        """
        # Check if x is already a torch.Tensor, if not, convert it
        if not isinstance(X, torch.Tensor):
            X = torch.Tensor(X)
        train_x = self.train_inputs[0]
        train_y = self.train_targets
        if not (X.dim() == train_x.dim()):
            X = X.unsqueeze(0)
        # Compute the covariance matrix K
        K = self.covar_module(train_x)
        # Compute the product of the inverse of K and train_y
        K_inv_g = gpytorch.solve(K, train_y)
        # Calculate the gradient of the covariance function with respect to x
        del_k = torch.pow(self.covar_module.base_kernel.lengthscale, -2) * (train_x - X) * self.covar_module(train_x, X)
        return K_inv_g @ del_k

    def get_gradient_auto(self):
        # based on https://colab.research.google.com/drive/1Byv0pLMuSFsqKtzHFaBf-8iR0PRJi7Es?usp=sharing#scrollTo
        # =YALVVS-iLBTP
        return torch.autograd.grad(self.observed_pred.mean, self.X, retain_graph=True)[0]


def split_data(
        X: "np.ndarray",
        y: "np.ndarray",
        train_size: "Union[float, int]",
        hidden_train_size: "Optional[float]" = None,
        bins: int = 1,
        random_state: "Any" = None,
) -> "Tuple[np.ndarray,np.ndarray,np.ndarray,np.ndarray]":
    """train_test_split based on bins
    The outputs are devided into [bins] bins, if one of the bins has
    less than [num_train/bins/hidden_test_size] samples, we select
    [hidden_test_size] of the samples in the bin.

    Args:
        X (np.ndarray): inputs
        y (np.ndarray): outputs
        num_train (Optional[int], optional): number of train sampels. Defaults to None.
        test_size (float, optional): ratio of the test set. Defaults to 0.8.
        bins (int, optional): number of bins. Defaults to 1.
        random_state (Any): random_state for sklearn.model_selection.train_test_split

    Returns:
        Tuple[np.ndarray,np.ndarray,np.ndarray,np.ndarray]: X_train, X_test, y_train, y_test
    """
    if train_size > 0 and train_size < 1:
        num_train = int(len(y) * train_size)
    else:
        num_train = train_size

    if hidden_train_size is None:
        hidden_train_size = Cfg["machine_learning"]["hidden_train_size"]

    Xs, ys = gen_bins(X, y, bins)
    avg_num_bins = num_train / bins
    upper_bound_bins = num_train / bins / hidden_train_size
    X_train, X_test, y_train, y_test, X_surplus, y_surplus = [], [], [], [], [], []
    current_len = 0
    for X_bin, y_bin in zip(Xs, ys):
        if len(y_bin) == 0:
            continue
        elif len(y_bin) == 1:
            X_train.append(X_bin)
            y_train.append(y_bin)
        elif len(y_bin) < upper_bound_bins:
            X_train_bin, X_test_bin, y_train_bin, y_test_bin = train_test_split(
                X_bin,
                y_bin,
                train_size=int(
                    np.clip(hidden_train_size * len(y_bin), 1.1, len(y_bin) - 0.9)
                ),
                random_state=random_state,
            )
            X_train.append(X_train_bin)
            X_surplus.append(X_test_bin)
            y_train.append(y_train_bin)
            y_surplus.append(y_test_bin)
            current_len += len(y_train_bin)
        else:
            X_train_bin, X_test_bin, y_train_bin, y_test_bin = train_test_split(
                X_bin,
                y_bin,
                train_size=int(np.clip(avg_num_bins, 1.1, len(y_bin) - 0.9)),
                random_state=random_state,
            )
            X_train.append(X_train_bin)
            X_surplus.append(X_test_bin)
            y_train.append(y_train_bin)
            y_surplus.append(y_test_bin)
            current_len += len(y_train_bin)

    if len(y_surplus) > 0:
        X_surplus = np.concatenate(X_surplus, axis=0)
        y_surplus = np.concatenate(y_surplus, axis=0)
        X_train_bin, X_test_bin, y_train_bin, y_test_bin = train_test_split(
            X_surplus,
            y_surplus,
            train_size=int(np.clip(num_train - current_len, 1.1, len(y_surplus) - 0.9)),
            random_state=random_state,
        )
        X_train.append(X_train_bin)
        X_test.append(X_test_bin)
        y_train.append(y_train_bin)
        y_test.append(y_test_bin)

    X_train = np.concatenate(X_train, axis=0)
    X_test = np.concatenate(X_test, axis=0)
    y_train = np.concatenate(y_train, axis=0)
    y_test = np.concatenate(y_test, axis=0)

    return X_train, X_test, y_train, y_test


def gen_bins(
        X: "np.ndarray", y: "np.ndarray", bins: int = 1
) -> "Tuple[List[np.ndarray],List[np.ndarray]]":
    Xs = []
    ys = []
    bin_edges = np.linspace(y.min(), y.max(), bins + 1)[1:]
    bin_ids = np.searchsorted(bin_edges, y)
    for bin_id in range(bins):
        Xs.append(X[bin_ids == bin_id])
        ys.append(y[bin_ids == bin_id])
    return Xs, ys


class ModelTrainer:
    @classmethod
    def create_from_config(cls, data_loader: "DataLoader") -> "ModelTrainer":
        return cls(data_loader=data_loader, **Cfg["gaussian_process"])

    def __init__(
            self,
            data_loader: "DataLoader",
            predicate_names: "List[str]",
            log_filename: str,
            n_binary: int = 1000,
            n_unary: int = 1000,
            training_iter: int = 100,
            suffix: str = "",
    ):
        self.data_loader = data_loader
        self.n_binary = n_binary
        self.n_unary = n_unary
        self.training_iter = training_iter
        self.predicate_names = predicate_names
        self.log_filename = log_filename
        self.suffix = suffix
        self._init_logger()

    @property
    def log_path(self) -> str:
        return os.path.join(Cfg["path"]["path_logs"], self.log_filename)

    def _init_logger(self) -> None:
        logger = logging.getLogger("model_trainer")
        if not getattr(logger, "is_initialized", False):
            logger.setLevel(logging.DEBUG)
            fileHandler = logging.FileHandler(self.log_path)
            fileHandler.setLevel(logging.DEBUG)
            formatter = logging.Formatter(
                "[%(asctime)s] <%(processName)-11s> {%(filename)s:%(lineno)d} %(levelname)s - %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
            )
            fileHandler.setFormatter(formatter)
            logger.addHandler(fileHandler)

            if Cfg["debug"]["debug_on"] and Cfg["debug"]["print_progress"]:
                consoleHandler = logging.StreamHandler()
                consoleHandler.setLevel(logging.DEBUG)
                consoleHandler.setFormatter(formatter)
                logger.addHandler(consoleHandler)

            logger.is_initialized = True

        self.logger = logger

    def train(self) -> None:
        for predicate_name in self.predicate_names:
            self.train_predicate(predicate_name)

    def train_predicate(self, predicate_name: str) -> None:
        log_msg = "training: " + predicate_name + self.suffix
        self.logger.info(log_msg)
        X, y = self.data_loader.Xy(predicate_name)
        arity = FeatureExtrator.get_arity(predicate_name)
        if arity == 1:
            n = self.n_unary
        else:
            n = self.n_binary
        if n >= len(y):
            X_train, y_train = X, y
            X_test, y_test = X, y
        else:
            X_train, X_test, y_train, y_test = split_data(
                X, y, n, random_state=1, bins=20
            )
        # train GPR
        train_X = torch.Tensor(X_train)
        train_y = torch.Tensor(y_train)
        model = self.train_model(train_X, train_y)
        self.dump_model(
            (model.state_dict(), train_X, train_y), predicate_name + self.suffix
        )

    def train_model(self, train_X, train_y) -> "ExactGPModel":
        likelihood = gpytorch.likelihoods.FixedNoiseGaussianLikelihood(
            torch.Tensor([0.001])
        )
        model = ExactGPModel(train_X, train_y, likelihood)

        train_X = train_X.to(_device)
        train_y = train_y.to(_device)
        model = model.to(_device)
        likelihood = likelihood.to(_device)

        model.train()
        likelihood.train()

        # Use the adam optimizer
        optimizer = torch.optim.Adam(
            model.parameters(), lr=0.1
        )  # Includes GaussianLikelihood parameters

        # "Loss" for GPs - the marginal log likelihood
        mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)

        warnings.filterwarnings("ignore")
        for i in range(self.training_iter):
            # Zero gradients from previous iteration
            optimizer.zero_grad()
            # Output from model
            output = model(train_X)
            # Calc loss and backprop gradients
            loss = -mll(output, train_y)
            loss.backward()
            if i % 100 == 0:
                self.logger.info(
                    "Iter %d/%d - Loss: %.3f  noise: %.3f"
                    % (
                        i + 1,
                        self.training_iter,
                        loss.item(),
                        model.likelihood.noise.item(),
                    )
                )
            optimizer.step()
        warnings.filterwarnings("default")
        return model

    @staticmethod
    def dump_model(model, file_name):
        torch.save(model, os.path.join(Cfg["path"]["path_models"], file_name + ".p"))


models = {}


def read_model(predicate_name) -> "ExactGPModel":
    if not predicate_name in models:
        state_dict, train_X, train_y = torch.load(
            os.path.join(Cfg["path"]["path_models"], predicate_name + ".p"),
            map_location=_device,
        )
        likelihood = gpytorch.likelihoods.FixedNoiseGaussianLikelihood(
            torch.Tensor([0.001])
        )
        model = ExactGPModel(train_X, train_y, likelihood)  # Create a new GP model
        model.load_state_dict(state_dict)
        model.eval()
        likelihood.eval()
        model.to(_device)
        models[predicate_name] = model
    return models[predicate_name]


class ModelEvaluator:
    @classmethod
    def create_from_config(cls, data_loader: "DataLoader") -> "ModelEvaluator":
        return cls(
            predicate_names=Cfg["gaussian_process"]["predicate_names"],
            data_loader=data_loader,
            n_binary=Cfg["gaussian_process"]["n_binary"],
            n_unary=Cfg["gaussian_process"]["n_binary"],
            suffix=Cfg["gaussian_process"]["suffix"],
        )

    def __init__(
            self,
            predicate_names: "List[str]",
            data_loader: "DataLoader",
            n_binary: int = 1000,
            n_unary: int = 1000,
            suffix: str = "",
    ) -> None:
        self.predicate_names = predicate_names
        self.data_loader = data_loader
        self.n_binary = n_binary
        self.n_unary = n_unary
        self.suffix = suffix

    def read_model(self, predicate_name) -> "ExactGPModel":
        return read_model(predicate_name + self.suffix)

    def evaluate(self) -> "Tuple[Figure, Axes, Dict[str, float]]":
        """plot the comparison between ground truth and prediction,
        and calculate mean squared error for all predicate models.

        Returns:
            Tuple[Figure, Axes, List[float]]: [fig, ax, mse]
        """
        rows = int(len(self.predicate_names) / 2 + 0.6)
        fig, axs = plt.subplots(rows, 2, figsize=(15, rows * 6))

        mse = dict()

        for i, predicate_name in enumerate(self.predicate_names):
            ax = axs.flat[i]
            model = self.read_model(predicate_name)

            X, y = self.data_loader.Xy(predicate_name)
            arity = FeatureExtrator.get_arity(predicate_name)
            if arity == 1:
                n = self.n_unary
            else:
                n = self.n_binary

            if n >= len(y):
                X_train, y_train = X, y
                X_test, y_test = X, y
            else:
                X_train, X_test, y_train, y_test = train_test_split(
                    X,
                    y,
                    test_size=1 - np.float128(n) / len(y),
                    random_state=1,
                )

            y_pred, std = model.predict(X_test)

            ax.scatter(y_test, y_pred, s=1)
            # ax.hist2d(y_test, y_pred, bins=20, norm=mcolors.PowerNorm(0.5))
            ax.plot([-1, 1], [-1, 1], "--k", alpha=0.2)
            ax.set_xlabel("ground truth")
            ax.set_ylabel("prediction")
            ax.set_title(predicate_name)

            mse[predicate_name] = mean_squared_error(y_test, y_pred)

        return fig, axs, mse

    def predict(self):
        y_preds = []
        stds = []

        for i, predicate_name in enumerate(self.predicate_names):
            model = self.read_model(predicate_name)

            X, y = self.data_loader.Xy(predicate_name)
            arity = FeatureExtrator.get_arity(predicate_name)
            if arity == 1:
                n = self.n_unary
            else:
                n = self.n_binary

            if n >= len(y):
                X_train, y_train = X, y
                X_test, y_test = X, y
            else:
                X_train, X_test, y_train, y_test = train_test_split(
                    X,
                    y,
                    test_size=1 - np.float128(n) / len(y),
                    random_state=1,
                )

            y_pred, std = model.predict(X_test)
            y_preds.append(y_pred)
            stds.append(std)

        return y_preds, stds


class PredicateEvaluatorML(PredicateEvaluator):
    def __init__(self, predicate_names: "List[str]") -> None:
        super().__init__(predicate_names)
        self.feature_extractor = FeatureExtrator(predicate_names)
        self.robustness_models = [
            read_model(predicate_name) for predicate_name in self.predicate_names
        ]
        self.list_feature_variables = None

    def evaluate_robustness(self, **kwargs) -> "Tuple[np.ndarray, np.ndarray]":
        """evaluate predicate robustness using machine learning models

        Args:
            world_state (WorldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            Tuple[np.ndarray, np.ndarray]: (robustness, std)

        """
        world_state, vehicles, states = parse(**kwargs)
        feature_variables = self.feature_extractor.extract_features(
            world_state=world_state, vehicles=vehicles, states=states
        )
        self.list_feature_variables = list_feature_variables = \
            FeatureExtrator.feature_dict_list_to_list_list(
                feature_variables
            )

        robustness = np.zeros_like(self.predicates)
        std = np.zeros_like(self.predicates)
        for i, robustness_model in enumerate(self.robustness_models):
            robustness[i], std[i] = robustness_model.predict(
                [list_feature_variables[i]]
            )
        characteristic_value = self.characteristic_function(
            world_state=world_state, vehicles=vehicles, states=states
        )

        veh_ids = []
        for v in vehicles:
            veh_ids.append(v.id)

        robustness[robustness * characteristic_value < 0] = Cfg["machine_learning"][
            "eps"
        ]

        robustness = np.copysign(
            np.clip(robustness, -1, 1).astype("float"),
            characteristic_value.astype("float"),
        )

        return robustness, std.astype("float")

    def derivative(self):
        dev_robustness = np.zeros_like(self.predicates)
        dev_robustness_auto = np.zeros_like(self.predicates)
        # todo: gradient of the std, currently not needed
        # dev_std = np.zeros_like(self.predicates)
        if self.list_feature_variables is None:
            assert AssertionError("the robustness needs to be firstly evaluated")
        for i, robustness_model in enumerate(self.robustness_models):
            # dev_robustness[i] = robustness_model.get_gradient(
            #     [self.list_feature_variables[i]]
            # )
            dev_robustness_auto[i] = robustness_model.get_gradient_auto(
            )
        return dev_robustness_auto
