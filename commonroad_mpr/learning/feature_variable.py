import enum
from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

import numpy as np
from commonroad.geometry.transform import rotate_translate
from commonroad_mpr.common import PredicateEvaluator, parse
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg

if TYPE_CHECKING:
    from typing import Dict, Iterable, List, Optional

    from commonroad_mpr.common import (
        Lane,
        Vehicle,
        VehicleState,
        VehicleTrajectory,
        World,
    )


class AbstractFeatureVariable(ABC):
    arity: int = 0
    names: "List[str]" = []

    @classmethod
    @abstractmethod
    def evaluate(cls, **kwargs) -> "Dict[str, float]":
        """evaluate feature variable

        Args:
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            Dict[str, float]: {name: value}
        """
        pass


class AbstractUnaryFeatureVariable(AbstractFeatureVariable):
    arity = 1


class AbstractBinaryFeatureVariable(AbstractFeatureVariable):
    arity = 2


class AbstractDistanceToLanes(ABC):
    @staticmethod
    def distance_to_lanes(
            x: float,
            y: float,
            occ_points: "Iterable[np.ndarray]",
            left_lane: "Lane",
            right_lane: "Lane",
    ) -> "List[float]":
        """compute distance from vehicle to lanes

        Args:
            x (float): vehicle position
            y (float): vehicle position
            occ_points (Iterable[np.ndarray]): positions of corners of vehicle
            left_lane (Lane): left most lane of the lanes
            right_lane (Lane): right most lane of the lanes

        Returns:
            List[float]: [
                min_dist_left,
                dist_left,
                max_dist_left,
                min_dist_right,
                dist_right,
                max_dist_right,
                dist_to_lanes,
                dist_to_lanes_min,
            ]
        """
        min_dist_left, max_dist_left = left_lane.min_max_distance_to_left(occ_points)
        min_dist_right, max_dist_right = right_lane.min_max_distance_to_right(
            occ_points
        )
        dist_left = left_lane.distance_to_left(x, y)
        dist_right = right_lane.distance_to_right(x, y)
        dist_to_lanes = min(max_dist_left, max_dist_right)
        dist_to_lanes_min = min(min_dist_left, min_dist_right)
        return [
            min_dist_left,
            dist_left,
            max_dist_left,
            min_dist_right,
            dist_right,
            max_dist_right,
            dist_to_lanes,
            dist_to_lanes_min,
        ]


class StateVariable(AbstractUnaryFeatureVariable):
    names = [
        "position",
        "velocity",
        "acceleration",
        "jerk",
        "jerk_dot",
        "lateral_position",
        "orientation",
        "curvature",
        "curvature_dot",
        "curvature_ddot",
    ]

    _parse_slots = [
        "s",
        "v",
        "a",
        "j",
        "j_dot",
        "d",
        "theta",
        "kappa",
        "kappa_dot",
        "kappa_ddot",
    ]

    @classmethod
    def evaluate(
            cls,
            state: "Optional[VehicleState]" = None,
            **kwargs,
    ) -> "Dict[str, float]":
        if state is None:
            _, _, states = parse(**kwargs)
            state = states[0]
        values = [getattr(state, name) for name in cls._parse_slots]
        return dict(zip(cls.names, values))


class VehicleFeature(AbstractUnaryFeatureVariable):
    names = ["length", "width"]

    @classmethod
    def evaluate(
            cls,
            vehicle: "Optional[Vehicle]" = None,
            **kwargs,
    ) -> "Dict[str, float]":
        if vehicle is None:
            _, vehicles, _ = parse(**kwargs)
            vehicle = vehicles[0]
        values = [getattr(vehicle, name) for name in cls.names]
        return dict(zip(cls.names, values))


class DistanceToLanes(AbstractUnaryFeatureVariable, AbstractDistanceToLanes):
    names = [
        "min_distance_to_road_left",
        "distance_to_road_left",
        "max_distance_to_road_left",
        "min_distance_to_road_right",
        "distance_to_road_right",
        "max_distance_to_road_right",
        "distance_to_road",
        "distance_to_road_min",
        "min_distance_to_ref_lane_left",
        "distance_to_ref_lane_left",
        "max_distance_to_ref_lane_left",
        "min_distance_to_ref_lane_right",
        "distance_to_ref_lane_right",
        "max_distance_to_ref_lane_right",
        "distance_to_ref_lane",
        "distance_to_ref_lane_min",
        "min_distance_to_occ_lanes_left",
        "distance_to_occ_lanes_left",
        "max_distance_to_occ_lanes_left",
        "min_distance_to_occ_lanes_right",
        "distance_to_occ_lanes_right",
        "max_distance_to_occ_lanes_right",
        "distance_to_occ_lanes",
        "distance_to_occ_lanes_min",
    ]

    @classmethod
    def evaluate(
            cls,
            **kwargs,
    ) -> "Dict[str, float]":
        world_state, vehicles, states = parse(**kwargs)
        state = states[0].get_state_in_world_frame()
        vehicle = vehicles[0]
        shape = world_state.scenario.obstacle_by_id(vehicle.id).obstacle_shape
        occ_points = list(
            rotate_translate(shape.vertices[:-1], [state.s, state.d], state.theta)
        )

        # road
        road_left_lane = world_state.road_network.left_most_real_lane
        road_right_lane = world_state.road_network.right_most_real_lane
        # ref
        ref_lane = vehicle.trajectory_persp.get_reference_lane(state.ts)
        # occ
        occ_lanes = vehicle.trajectory_persp.get_occupied_lanes(state.ts)
        occ_left_lane = world_state.road_network.left_most_lane(occ_lanes)[0]
        occ_right_lane = world_state.road_network.right_most_lane(occ_lanes)[0]

        values = cls.distance_to_lanes(
            state.s, state.d, occ_points, road_left_lane, road_right_lane
        )
        values += cls.distance_to_lanes(
            state.s, state.d, occ_points, ref_lane, ref_lane
        )
        values += cls.distance_to_lanes(
            state.s, state.d, occ_points, occ_left_lane, occ_right_lane
        )

        return dict(zip(cls.names, values))


class RelativeStateVariable(AbstractBinaryFeatureVariable):
    names = [
        "distance",
        "lateral_distance",
        "relative_longitudinal_velocity",
        "relative_lateral_velocity",
    ]

    @classmethod
    def evaluate(
            cls,
            ego_state: "Optional[VehicleState]" = None,
            other_state: "Optional[VehicleState]" = None,
            **kwargs,
    ) -> "Dict[str, float]":
        if ego_state is None or other_state is None:
            _, _, states = parse(**kwargs)
            ego_state, other_state = states[:2]
        values = [
            other_state.rear_s - ego_state.front_s,
            # todo: left_d
            other_state.d - ego_state.d,
            other_state.v * np.cos(other_state.theta)
            - ego_state.v * np.cos(ego_state.theta),
            other_state.v * np.sin(other_state.theta)
            - ego_state.v * np.sin(ego_state.theta),
        ]
        return dict(zip(cls.names, values))


class RelativeDistanceToLanes(AbstractBinaryFeatureVariable, AbstractDistanceToLanes):
    names = [
        "relative_min_distance_to_ref_lane_left",
        "relative_distance_to_ref_lane_left",
        "relative_max_distance_to_ref_lane_left",
        "relative_min_distance_to_ref_lane_right",
        "relative_distance_to_ref_lane_right",
        "relative_max_distance_to_ref_lane_right",
        "relative_distance_to_ref_lane",
        "relative_distance_to_ref_lane_min",
        "relative_min_distance_to_occ_lanes_left",
        "relative_distance_to_occ_lanes_left",
        "relative_max_distance_to_occ_lanes_left",
        "relative_min_distance_to_occ_lanes_right",
        "relative_distance_to_occ_lanes_right",
        "relative_max_distance_to_occ_lanes_right",
        "relative_distance_to_occ_lanes",
        "relative_distance_to_occ_lanes_min",
    ]

    @classmethod
    def evaluate(
            cls,
            **kwargs,
    ) -> "Dict[str, float]":
        world_state, vehicles, states = parse(**kwargs)
        ego_state = states[0].get_state_in_world_frame()
        ego_vehicle, other_vehicle = vehicles[:2]
        shape = world_state.scenario.obstacle_by_id(ego_vehicle.id).obstacle_shape
        occ_points = list(
            rotate_translate(
                shape.vertices[:-1], [ego_state.s, ego_state.d], ego_state.theta
            )
        )

        # ref
        ref_lane = other_vehicle.trajectory_persp.get_reference_lane(ego_state.ts)
        # occ
        occ_lanes = other_vehicle.trajectory_persp.get_occupied_lanes(ego_state.ts)
        occ_left_lane = world_state.road_network.left_most_lane(occ_lanes)[0]
        occ_right_lane = world_state.road_network.right_most_lane(occ_lanes)[0]

        values = cls.distance_to_lanes(
            ego_state.s, ego_state.d, occ_points, ref_lane, ref_lane
        )
        values += cls.distance_to_lanes(
            ego_state.s, ego_state.d, occ_points, occ_left_lane, occ_right_lane
        )

        return dict(zip(cls.names, values))


@enum.unique
class FeatureVariables(enum.Enum):
    STATE_VARIABLE = StateVariable
    VEHICLE_FEATURE = VehicleFeature
    DISTANCE_TO_LANES = DistanceToLanes
    RELATIVE_STATE_VARIABLE = RelativeStateVariable
    RELATIVE_DISTANCE_TO_LANES = RelativeDistanceToLanes


class FeatureExtrator:
    Features: "List[AbstractFeatureVariable]" = [e.value for e in FeatureVariables]

    @classmethod
    @property
    def all_valid_features(cls) -> "Dict[int, List[str]]":
        res = {}
        for Feature in cls.Features:
            if Feature.arity not in res:
                res[Feature.arity] = list(Feature.names)
            else:
                res[Feature.arity] += Feature.names
        return res

    @classmethod
    @property
    def all_feature_variable_names(cls) -> "Dict[str, List[str]]":
        arity_names = cls.all_valid_features
        return {
            "ego": arity_names[1],
            "other": arity_names[1],
            "ego_other": arity_names[2],
            "other_ego": arity_names[2],
        }

    @classmethod
    def feature_classes_by_arity(cls, arity: int) -> "List[AbstractFeatureVariable]":
        return [f for f in cls.Features if f.arity == arity]

    def __init__(self, predicate_names: "List[str]"):
        self.predicate_names = predicate_names
        self.predicate_evaluator = PredicateEvaluator(predicate_names=predicate_names)

    @classmethod
    def all_feature_variables(cls, **kwargs) -> "Dict[str, Dict[str, float]]":
        """get all possible feature variables
        Args:
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            Dict[str, Dict[str, float]]: {vehicle: feature_name: value}
        """
        world_state, vehicles, states = parse(**kwargs)
        params = {"world_state": world_state, "vehicles": vehicles, "states": states}
        num_vehicles = len(vehicles)
        if num_vehicles == 1:
            return {"ego": cls.unary_feature_variables(params)}
        else:  # num_vehicles == 2
            reverse_params = {
                "world_state": world_state,
                "vehicles": list(reversed(vehicles)),
                "states": list(reversed(states)),
            }
            return {
                "ego": cls.unary_feature_variables(params),
                "other": cls.unary_feature_variables(reverse_params),
                "ego_other": cls.binary_feature_variables(params),
                "other_ego": cls.binary_feature_variables(reverse_params),
            }

    @classmethod
    def unary_feature_variables(
            cls, params: "Dict[str, World|List[VehicleState|Vehicle]]"
    ) -> "Dict[str, float]":
        state: "VehicleState" = params["states"][0]
        trajectory: "VehicleTrajectory" = state.get_parent_recursively_as_type(
            "VehicleTrajectory"
        )
        if not trajectory.has_feature_variable_assignment(state.ts):
            feature_classes = cls.feature_classes_by_arity(1)
            result = {}
            for feature_class in feature_classes:
                if feature_class.__name__ == "StateVariable":
                    feature_class: "StateVariable"
                    temp = feature_class.evaluate(state)
                elif feature_class.__name__ == "VehicleFeature":
                    feature_class: "VehicleFeature"
                    temp = feature_class.evaluate(params["vehicles"][0])
                else:
                    temp = feature_class.evaluate(**params)
                result.update(temp)

            trajectory.assign_feature_variable(state.ts, result)
        return trajectory.get_feature_variable(state.ts)

    @classmethod
    def binary_feature_variables(
            cls, params: "Dict[str, World|List[VehicleState|Vehicle]]"
    ) -> "Dict[str, float]":
        feature_classes = cls.feature_classes_by_arity(2)
        ego_state, other_state = params["states"][:2]
        result = {}
        for feature_class in feature_classes:
            if feature_class.__name__ == "RelativeStateVariable":
                feature_class: "RelativeStateVariable"
                temp = feature_class.evaluate(ego_state, other_state)
            else:
                temp = feature_class.evaluate(**params)
            result.update(temp)
        return result

    @staticmethod
    def featrue_dict_to_list(
            dict_featrue: "Dict[str, Dict[str, float]]",
    ) -> "List[float]":
        return [
            value for subdict in dict_featrue.values() for value in subdict.values()
        ]

    @classmethod
    def feature_dict_list_to_list_list(
            cls, list_dict_feature: "List[Dict[str, Dict[str, float]]]"
    ) -> "List[List[float]]":
        return [
            cls.featrue_dict_to_list(dict_feature) for dict_feature in list_dict_feature
        ]

    @classmethod
    def feature_dict_to_list_keys(cls, feature_dict: "Dict[str, Dict[str, float]]") -> "List[str]":
        # Flatten dict keys, assuming no key is repeated across nested dictionaries
        flat_keys = [f"{outer_key}_{inner_key}" for outer_key, inner_dict in feature_dict.items()
                     for inner_key in inner_dict.keys()]
        return flat_keys

    @classmethod
    def feature_dict_list_to_list_list_keys(
            cls, list_dict_feature: "List[Dict[str, Dict[str, float]]]"
    ) -> "List[List[str]]":
        return [
            cls.feature_dict_to_list_keys(dict_feature) for dict_feature in list_dict_feature
        ]

    def extract_features(self, **kwargs) -> "List[Dict[str, Dict[str, float]]]":
        """get feature variables for target predicates
        Args:
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            List[Dict[str, Dict[str, float]]]: [feature variables for predicate in self.predicate_names]
        """
        world_state, vehicles, states = parse(**kwargs)
        all_features = self.all_feature_variables(
            world_state=world_state, vehicles=vehicles, states=states
        )
        predicate_evaluation = self.predicate_evaluator.characteristic_function(
            world_state=world_state, vehicles=vehicles, states=states
        )

        res = []
        for i, predicate_name in enumerate(self.predicate_names):

            desired_features = self.get_desired_features(predicate_name)
            res.append(
                {
                    vehicle: {
                        name: all_features[vehicle][name] for name in desired_names
                    }
                    for vehicle, desired_names in desired_features.items()
                    if desired_names is not None
                }
            )

            desired_predicate_evaluation = self.get_desired_predicate_evaluation(
                predicate_name
            )
            if "bool" in desired_predicate_evaluation:
                res[i].update(
                    {"predicate_evaluation": {"bool": predicate_evaluation[i]}}
                )
        return res

    @classmethod
    def get_desired_features(cls, predicate_name: str) -> "Dict[str, str]":
        predicate_param: "Dict" = Cfg["feature_variable"][predicate_name]
        arity: int = predicate_param["arity"]
        if predicate_param.get(
                "all_features_desired",
                Cfg["feature_variable"]["default"]["all_features_desired"],
        ):
            desired_features = cls.all_feature_variable_names
        else:
            desired_features = dict(
                predicate_param.get(
                    "desired_features",
                    Cfg["feature_variable"]["default"]["desired_features"],
                )
            )
        if arity == 1:
            desired_features.pop("other", None)
            desired_features.pop("ego_other", None)
            desired_features.pop("other_ego", None)
        return desired_features

    @classmethod
    def get_desired_predicate_evaluation(cls, predicate_name: str) -> "List[str]":
        predicate_param: "Dict" = Cfg["feature_variable"][predicate_name]
        desired_predicate_evaluation = predicate_param.get(
            "desired_predicate_evaluation",
            Cfg["feature_variable"]["default"]["desired_predicate_evaluation"],
        )

        if desired_predicate_evaluation is not None:
            desired_predicate_evaluation = list(desired_predicate_evaluation)
        else:
            desired_predicate_evaluation = []

        return desired_predicate_evaluation

    @staticmethod
    def get_arity(predicate_name: str) -> int:
        predicate_param: "Dict" = Cfg["feature_variable"][predicate_name]
        return predicate_param["arity"]
