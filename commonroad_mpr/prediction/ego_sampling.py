import itertools
from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

import commonroad_dc.feasibility.feasibility_checker as feasibility_checker
import numpy as np
from commonroad_mpr.common import TrajectoryPerspective
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg
from scipy.stats import norm, uniform

if TYPE_CHECKING:
    from typing import Dict, Iterable, List, Optional, Tuple, Union

    from commonroad_mpr.common import Vehicle, VehicleState, World
    from matplotlib.axes import Axes
    from matplotlib.figure import Figure
    from omegaconf import DictConfig, ListConfig
    from vehiclemodels.vehicle_parameters import VehicleParameters


class Sampling1D(ABC):
    @classmethod
    def assert_input(cls, min=0, max=1, size=1, eps=0.0015):
        assert max > min, "max must be greater than min."
        assert 0 < eps and eps < 0.5, "eps must be between 0 and 0.5."
        assert isinstance(size, int), "size must be an integer."
        assert size >= 1, "size must be greater than or equal to 1."

    @classmethod
    @abstractmethod
    def grid_sampling(cls, min=0, max=1, size=1, eps=0.0015):
        pass

    @classmethod
    @abstractmethod
    def monte_carlo_sampling(cls, min=0, max=1, size=1, eps=0.0015):
        pass


class UniformSampling1D(Sampling1D):
    @classmethod
    def grid_sampling(cls, min=0, max=1, size=1, eps=0.0015):
        cls.assert_input(min, max, size, eps)
        if size == 1:
            return np.array([(min + max) / 2])
        else:  # size > 1
            return np.linspace(min, max, size)

    @classmethod
    def monte_carlo_sampling(cls, min=0, max=1, size=1, eps=0.0015):
        cls.assert_input(min, max, size, eps)
        x = uniform.rvs(
            loc=min,
            scale=(max - min),
            size=size,
        )
        return x


class NormalSampling1D(Sampling1D):
    @classmethod
    def grid_sampling(cls, min=0, max=1, size=1, eps=0.0015):
        cls.assert_input(min, max, size, eps)
        cdfs = np.linspace(eps, 1 - eps, size)
        x = norm.ppf(
            cdfs, loc=(min + max) / 2, scale=1 / -norm.ppf(eps) * (max - min) / 2
        )
        # check small overflow
        if x[0] < min:
            x[0] = min
        if x[-1] > max:
            x[-1] = max
        return x

    @classmethod
    def monte_carlo_sampling(cls, min=0, max=1, size=1, eps=0.0015):
        cls.assert_input(min, max, size, eps)
        x = norm.rvs(
            loc=(min + max) / 2,
            scale=1 / -norm.ppf(eps) * (max - min) / 2,
            size=size,
        )
        return x


class SamplingXD:
    def __init__(self, distribution: str, simulation: str) -> None:
        sampler1d: Sampling1D
        self.sample_function: classmethod
        self.distribution: str = distribution
        self.simulation: str = simulation

        if distribution == "uniform":
            sampler1d = UniformSampling1D
        else:  # 'normal'
            sampler1d = NormalSampling1D

        if simulation == "grid":
            self.sample_function = sampler1d.grid_sampling
        else:  # 'monte-carlo'
            self.sample_function = sampler1d.monte_carlo_sampling

    def sample(self, dim=1, min=[0], max=[1], size=[1], number=1, eps=[0.0015]):
        self.assert_input(dim, min, max, size, number, eps)
        dim, min, max, size, eps = self.process_input(dim, min, max, size, eps)

        samples = []
        if self.simulation == "grid":
            for i in range(dim):
                samples.append(self.sample_function(min[i], max[i], size[i], eps[i]))
            return itertools.product(*samples)
        else:  # 'monte-carlo'
            for _ in range(number):
                sample = []
                for i in range(dim):
                    sample.append(self.sample_function(min[i], max[i], 1, eps[i]))
                samples.append(sample)
            return samples


    def assert_input(self, dim=1, min=[0], max=[1], size=[1], number=1, eps=[0.0015]):
        """check whether lengths of input variables match the dimention"""
        assert isinstance(dim, int), "dim must be an integer."
        assert isinstance(number, int), "number must be an integer."
        assert hasattr(min, "__getitem__"), "min must be indexable."
        assert hasattr(max, "__getitem__"), "max must be indexable."
        assert hasattr(size, "__getitem__"), "size must be indexable."
        assert hasattr(eps, "__getitem__"), "eps must be indexable."
        assert dim >= 1, "dim must be greater than or equal to 1."
        assert (
            len(min) == 1 or len(min) == dim
        ), f"length of min doesn't match the dimention {dim}."
        assert (
            len(max) == 1 or len(max) == dim
        ), f"length of max doesn't match the dimension {dim}."
        if self.simulation == "grid":
            assert (
                len(size) == 1 or len(size) == dim
            ), f"length of size doesn't match the dimension {dim}."
        else:  # 'monte-carlo'
            assert number >= 1, \
                f"should be enough samples provided, i.e., >= 1 instead of {number}."
        assert (
            len(eps) == 1 or len(eps) == dim
        ), f"length of eps doesn't match the dimension {dim}."

    def process_input(self, dim=1, min=[0], max=[1], size=[1], eps=[0.0015]):
        if len(min) != dim:
            min = min * dim
        if len(max) != dim:
            max = max * dim
        if len(size) != dim:
            size = size * dim
        if len(eps) != dim:
            eps = eps * dim
        return dim, min, max, size, eps


class Polynomial:
    @classmethod
    def from_boundary(
        cls,
        left: "List[Optional[float]]",
        right: "List[Optional[float]]",
        interval: float,
    ) -> np.polynomial.Polynomial:
        """generate polynomial function y=f(x) with boundary conditions

        Args:
            left (list[Optional[ float]]): left boundary conditions,
                [y,y',y'',...] | x=0, None means free.
            right (list[Optional[ float]]): right boundary conditions,
                [y,y',y'',...] | x=t, None means free.
            interval (float): t.

        Returns:
            numpy.polynomial.Polynomial: the polynomial function.
        """
        degree = sum(x is not None for x in left + right) - 1
        order = max(len(left), len(right)) - 1
        t = cls.powers(interval, degree)
        derivative_parameter = cls.get_derivative_parameter(degree, order)
        A, b = cls.get_linear_system(derivative_parameter, left, right, t)
        coef = np.linalg.solve(A, b)
        return np.polynomial.Polynomial(coef)

    @staticmethod
    def get_derivative_parameter(degree: int, order: int) -> np.ndarray:
        """get the matirx containing parameters of derivative of order [0 to order].
        The i-th order derivative of polynomial function at x is calculated by multiplying
        the i-th row of the matrix with transpose of powers(x,degree).

        Args:
            degree (int): degree of the polynomial
            order (int): max necessary order of the derivatives

        Returns:
            numpy.ndarray: the derivative parameter matirx
        """
        res = np.ones((1, degree + 1))
        if order == 0:
            return res

        multiplier = np.arange(degree + 1).reshape((1, -1))
        for i in range(order):
            res = np.concatenate([res, res[i] * multiplier], axis=0)
            multiplier[multiplier > 0] -= 1

        return res

    @staticmethod
    def powers(x: float, n: int) -> np.ndarray:
        """get the array containing powers of x

        Args:
            x (float): base
            n (int): max exponent

        Returns:
            numpy.ndarray: powers of x, [x^0, x^1, ..., x^n]
        """
        res = [1]
        for i in range(n):
            res.append(res[i] * x)
        return np.array(res)

    @classmethod
    def get_linear_system(
        cls,
        derivative_parameter: np.ndarray,
        left: "List[Optional[float]]",
        right: "List[Optional[float]]",
        t: np.ndarray,
    ) -> "Tuple[np.ndarray, np.ndarray]":
        """construct the linear system to solve the coefficients of the polynomial function

        Args:
            derivative_parameter (numpy.ndarray): derivative parameter matirx
            left (list[Optional[ float]]): left boundary conditions
            right (list[Optional[ float]]): right boundary conditions
            t (numpy.ndarray): powers of t

        Returns:
            Tuple[numpy.ndarray,numpy.ndarray]: A, b for linear system A * x = b
        """
        A_left, b_left = cls.get_linear_system_left(derivative_parameter, left)
        A_right, b_right = cls.get_linear_system_right(derivative_parameter, right, t)
        A = np.concatenate([A_left, A_right], axis=0)
        b = np.concatenate([b_left, b_right], axis=0)
        return A, b

    @classmethod
    def get_linear_system_left(
        cls, derivative_parameter: np.ndarray, left: "List[Optional[float]]"
    ) -> "Tuple[np.ndarray, np.ndarray]":
        """construct half the linear system considering the left condition

        Args:
            derivative_parameter (numpy.ndarray): derivative parameter matirx
            left (list[Optional[ float]]): left boundary conditions

        Returns:
            Tuple[numpy.ndarray,numpy.ndarray]: A, b for the half linear system
        """
        diag = np.diag(derivative_parameter)
        A = np.zeros(derivative_parameter.shape)
        np.fill_diagonal(A, diag)
        b = np.array(left).reshape([-1, 1])
        return cls.drop_none(A, b)

    @classmethod
    def get_linear_system_right(
        cls,
        derivative_parameter: np.ndarray,
        right: "List[Optional[float]]",
        t: np.ndarray,
    ) -> "Tuple[np.ndarray, np.ndarray]":
        """construct half the linear system considering the right condition

        Args:
            derivative_parameter (numpy.ndarray): derivative parameter matirx
            right (list[Optional[ float]]): right boundary conditions
            t (numpy.ndarray): powers of t

        Returns:
            Tuple[numpy.ndarray,numpy.ndarray]: A, b for the half linear system
        """

        A = derivative_parameter.copy()
        for i, row in enumerate(A):
            row *= np.roll(t, i)
        b = np.array(right, dtype=object).reshape([-1, 1])
        return cls.drop_none(A, b)

    @staticmethod
    def drop_none(A: np.ndarray, b: np.ndarray) -> "Tuple[np.ndarray, np.ndarray]":
        """clean the linear system by dropping the columns containing free boundary conditions

        Args:
            A (numpy.ndarray): A for the linear system
            b (numpy.ndarray): b for the linear system

        Returns:
            Tuple[numpy.ndarray,numpy.ndarray]: A, b for the cleaned linear system
        """
        b = b.astype("float64")
        A = A[0 : len(b), :]
        A = A[(~np.isnan(b)).flatten(), :]
        b = b[~np.isnan(b)]
        return A, b


class AbstractFutureStateSampler(ABC):
    def __init__(
        self,
        vehicle: "Vehicle",
        initial_time_step: int,
        time_horizon: float,
        end_state_options: "Dict",
        samplingxd: "Union[DictConfig, ListConfig]",
    ):
        self.time_horizon: float = time_horizon
        self.vehicle = vehicle
        self.world_state: "World" = self.vehicle.parent()
        self.dt: float = self.world_state.dt
        self.end_state_options = end_state_options
        self.initial_time_step = initial_time_step
        self.sampler_params = samplingxd
        self.sampler: SamplingXD = SamplingXD(**self.sampler_params)

    @property
    def num_ts(self) -> int:
        return self.compute_num_ts(self.time_horizon, self.dt)

    @classmethod
    def compute_num_ts(cls, time_horizon: float, dt: float) -> int:
        return int(time_horizon / dt + Cfg["sampling_approach"]["eps"])

    @abstractmethod
    def sample(self) -> "Iterable[VehicleState]":
        """yields sampled vehicle states"""
        pass


class StateBasedSampling(AbstractFutureStateSampler):
    @property
    def hash(self) -> str:
        return "SBS_" + str(self.initial_time_step) + "_" + str(self.num_ts)

    @property
    def has_cache(self) -> bool:
        return self.vehicle.has_cache(self.hash)

    @property
    def cache(self) -> "StateBasedSamplingResult":
        return self.vehicle.get_cache(self.hash)

    def get_sample_params(
        self,
        long_left: "List[float]",
        lat_left: "List[float]",
    ) -> "Dict[str, Union[int, List[Union[int, float]]]]":
        vehicle_params: "VehicleParameters" = self.vehicle.vehicle_dynamics.parameters
        params = {
            "dim": sum(len(l) for l in self.end_state_options["sample_long_lat"]),
            "size": self.end_state_options["size"],
            "number": self.end_state_options["number"],
            "min": [],
            "max": [],
        }
        for s_order in self.end_state_options["sample_long_lat"][0]:
            if s_order == 0:
                raise NotImplementedError
            elif s_order == 1:
                params["min"].append(
                    long_left[1] - vehicle_params.longitudinal.a_max * self.time_horizon
                )
                params["max"].append(
                    long_left[1] + vehicle_params.longitudinal.a_max * self.time_horizon
                )
            elif s_order == 2:
                raise NotImplementedError
            else:
                raise Exception("order greater than 2 not supported.")

        for d_order in self.end_state_options["sample_long_lat"][1]:
            if d_order == 0:
                params["min"].append(lat_left[0] - self.end_state_options["d_radius"])
                params["max"].append(lat_left[0] + self.end_state_options["d_radius"])
            elif d_order == 1:
                params["min"].append(-self.end_state_options["d_dot_radius"])
                params["max"].append(self.end_state_options["d_dot_radius"])
            elif d_order == 2:
                raise NotImplementedError
            else:
                raise Exception("order greater than 2 not supported.")
        return params

    def parse_sample(
        self, sample: "Tuple[float]"
    ) -> "Tuple[List[Optional[float]], List[Optional[float]]]":
        long_lat_right = ([], [])
        sample = list(sample)
        for l in range(2):
            for i in range(3):
                if i in self.end_state_options["sample_long_lat"][l]:
                    long_lat_right[l].append(sample.pop(0))
                elif i in self.end_state_options["zero_long_lat"][l]:
                    long_lat_right[l].append(0)
                else:
                    long_lat_right[l].append(None)
        return long_lat_right

    def is_valid_end_state(
        self, long_right: "List[float]", lat_right: "List[float]"
    ) -> bool:
        # no backwards
        if long_right[1] <= 0.001:
            return False
        # max velocity
        vehicle_params: "VehicleParameters" = self.vehicle.vehicle_dynamics.parameters
        velocity = np.linalg.norm([long_right[1], lat_right[1]])
        if velocity > vehicle_params.longitudinal.v_max:
            return False
        # max acceleration
        acceleration = np.linalg.norm([long_right[2], lat_right[2]])
        if acceleration > vehicle_params.longitudinal.a_max:
            return False
        return True

    def end_state_sample(
        self, sample_params: "Dict[str, Union[int, List[Union[int, float]]]]"
    ) -> "Iterable[Tuple[List[Optional[float]], List[Optional[float]]]]":
        for sample in self.sampler.sample(**sample_params):
            long_right, lat_right = self.parse_sample(sample)
            # prefilter
            if self.is_valid_end_state(long_right, lat_right):
                yield long_right, lat_right

    def get_start_state(self) -> "Tuple[List[float], List[float]]":
        return self.vehicle.trajectory_persp.get_long_lat_state(self.initial_time_step)

    def sample(self) -> "Iterable[VehicleState]":
        if not self.has_cache:
            self.generate_cache()
        return self.cache.to_iter()

    def generate_cache(self) -> None:
        # get start states
        long_left, lat_left = self.get_start_state()

        # sample end states
        sample_params = self.get_sample_params(long_left, lat_left)
        result = []
        for long_right, lat_right in self.end_state_sample(sample_params):

            # interpolate
            long_fun = Polynomial.from_boundary(
                long_left, long_right, self.time_horizon
            )
            lat_fun = Polynomial.from_boundary(lat_left, lat_right, self.time_horizon)

            # transfer back to cartesian coord
            trajectory_persp = TrajectoryPerspective.create_from_functions(
                long_fun,
                lat_fun,
                self.vehicle,
                self.initial_time_step,
                self.num_ts,
            )

            initial_state = self.world_state.scenario.obstacle_by_id(
                self.vehicle.id
            ).state_at_time(self.initial_time_step)
            commonroad_input = trajectory_persp.convert_to_commonroad_input_trajectory()

            # check feasibility
            feasible, _ = feasibility_checker.input_vector_feasibility(
                initial_state,
                commonroad_input,
                self.vehicle.vehicle_dynamics,
                self.dt,
            )

            if feasible:
                result.append(trajectory_persp)

        # store result
        self.vehicle.append_cache(self.hash, StateBasedSamplingResult(result))

    def plot_result(self, **kwargs) -> "Tuple[Figure, Axes]":
        if not self.has_cache:
            self.generate_cache()
        fig, ax = self.world_state.plot(self.initial_time_step, **kwargs)
        self.cache.plot(ax)
        return fig, ax


class StateBasedSamplingResult:
    def __init__(self, trajectory_persps: "List[TrajectoryPerspective]") -> None:
        self.trajectory_persps = trajectory_persps
        if len(trajectory_persps) != 0:
            initial_time_step = trajectory_persps[0].initial_time_step
            self.clcs_id = trajectory_persps[0].get_reference_lane(initial_time_step).id

    def to_iter(self) -> "Iterable[VehicleState]":
        for trajectory_persp in self.trajectory_persps:
            for time_step in range(
                trajectory_persp.initial_time_step, trajectory_persp.final_time_step + 1
            ):
                yield trajectory_persp.trajectory_by_clcs_id(
                    self.clcs_id
                ).state_at_time_step(time_step)

    def plot(self, ax):
        for trajectory_persp in self.trajectory_persps:
            trajectory_persp.plot(
                ax, **Cfg["visualization"]["state_based_sampling_result"]
            )
