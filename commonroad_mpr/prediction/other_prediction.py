from commonroad_mpr.common import Vehicle, VehicleState


class RecordedData:
    def __init__(self, vehicle: "Vehicle") -> None:
        self.vehicle = vehicle

    def corrsponding_state(self, ego_state: "VehicleState") -> "VehicleState":
        return self.vehicle.trajectory_persp.trajectory_by_clcs_id(
            ego_state.clcs_id
        ).state_at_time_step(ego_state.ts)

    @staticmethod
    def assert_validity(vehicle: "Vehicle", initial_time_step: int, num_ts: int):
        assert None in vehicle.trajectory_persp.clcs_ids, "Trajectory not valid."
        assert (
            vehicle.initial_time_step <= initial_time_step
        ), "Trajectory of the other vehicle begins after initial time step."
        final_time_step = initial_time_step + num_ts
        assert (
            vehicle.final_time_step >= final_time_step
        ), "Trajectory of the other vehicle ends before final time step."
