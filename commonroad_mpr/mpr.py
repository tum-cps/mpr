from typing import TYPE_CHECKING

import numpy as np
from commonroad_mpr.prediction import RecordedData
from commonroad_mpr.common import parse
from commonroad_mpr.prediction import StateBasedSampling
from commonroad_mpr.common import PredicateEvaluator
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg
from commonroad_mpr.utils.visualization import save_figure

if TYPE_CHECKING:
    from typing import List, Dict


class ModelPredictiveRobustness:
    def __init__(self, predicate_names: "List[str]", normalize: bool = False) -> None:
        self.predicate_evaluator = PredicateEvaluator(predicate_names)
        self.predicate_names = self.predicate_evaluator.predicate_names
        self.arity = self.predicate_evaluator.arity
        self.ego_sampler: StateBasedSampling = None
        self.other_behavior_model: RecordedData = None
        self.normalize = normalize

    def evaluate(
        self, param_fig: "Dict" = {}, **kwargs
    ) -> "Dict[str, float | Dict[str, float]]":
        """evaluate robustness

        Args:
            param_fig (Dict):
                plot configs
                {
                    fig_name: str,
                    kwargs: Dict, plot parameters
                }
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            Dict[str, Dict[str, float]]:
                {predicate_name: {
                    "count_valid": int,
                    "count_true": int,
                    "count_error": int,
                    "boolean": 0 | 1,
                    "robustness": float,
                    }
                } model predictive robustness of the predicates

        """
        # parse inputs, create sampler and behavior model
        world_state, vehicles, states = parse(**kwargs)
        time_step = states[0].ts

        self.ego_sampler = StateBasedSampling(
            vehicles[0],
            time_step,
            **Cfg["sampling_approach"]["state_based_sampling"],
        )

        if self.arity == 2:
            assert (
                len(vehicles) >= 2
            ), f"There exists binary predicates, but {len(vehicles)} vehicles given."
            other_vehicle = vehicles[1]
            RecordedData.assert_validity(other_vehicle, time_step, self.ego_sampler.num_ts)
            self.other_behavior_model = RecordedData(other_vehicle)

        # model predictive approach
        count_valid = np.zeros_like(self.predicate_evaluator.predicates)
        count_true = np.zeros_like(self.predicate_evaluator.predicates)
        count_error = np.zeros_like(self.predicate_evaluator.predicates)
        for ego_future_state in self.ego_sampler.sample():
            try:
                future_states = [ego_future_state]
                if self.other_behavior_model is not None:
                    future_states.append(
                        self.other_behavior_model.corrsponding_state(ego_future_state)
                    )

                for i, characteristic_value in enumerate(
                    self.predicate_evaluator.characteristic_function(
                        world_state=world_state,
                        vehicles=vehicles,
                        states=future_states,
                    )
                ):
                    if characteristic_value == 1:
                        count_true[i] += 1
                    count_valid[i] += 1

            except:
                count_error += 1

        # boolean evaluation
        characteristic_value = self.predicate_evaluator.characteristic_function(
            world_state=world_state, vehicles=vehicles, states=states
        )

        # compute robustness
        probability = count_true / (count_valid + Cfg["robustness"]["eps"])
        satisfied = characteristic_value>0
        probability[~satisfied] = 1-probability[~satisfied]

        if self.normalize:
            robustness = self.normalize_robustness(probability, characteristic_value)
        else:
            robustness = np.sign(characteristic_value) * probability
        # plot
        if Cfg["debug"]["debug_on"] and Cfg["debug"]["save_plots"]:
            fig_name = param_fig.get("fig_name", "sbs")
            fig, _ = self.ego_sampler.plot_result(**param_fig.get("kwargs", {}))
            save_figure(fig, fig_name + ".png")

        # return
        result = {}
        for i, predicate_name in enumerate(self.predicate_names):
            values = [
                count_valid[i],
                count_true[i],
                count_error[i],
                characteristic_value[i],
                robustness[i],
            ]
            result[predicate_name] = dict(
                zip(Cfg["robustness"]["output_names"], values)
            )
        return result

    def normalize_robustness(self, probability, characteristic):
        normalize_parameters = {}
        for key in ["pos_max", "pos_min", "neg_max", "neg_min"]:
            values = [Cfg["robustness"]["norm_params"][predicate_name][key] for predicate_name in self.predicate_names]
            normalize_parameters[key] = np.array(values)
        
        # normalize
        satisfied = characteristic>0
        robustness = np.zeros_like(probability)
        robustness[satisfied] = (probability[satisfied] - normalize_parameters["pos_min"][satisfied])/(
                    normalize_parameters["pos_max"][satisfied] - normalize_parameters["pos_min"][satisfied])
        robustness[~satisfied] = - (probability[~satisfied] - normalize_parameters["neg_min"][~satisfied])/(
                    normalize_parameters["neg_max"][~satisfied] - normalize_parameters["neg_min"][~satisfied])
        
        return robustness
