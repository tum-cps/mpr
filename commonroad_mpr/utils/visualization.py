import os
import warnings
from typing import TYPE_CHECKING

import matplotlib.pyplot as plt
from commonroad.common.util import Interval
from commonroad.scenario.scenario import Scenario
from commonroad.visualization.mp_renderer import MPRenderer
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg

if TYPE_CHECKING:
    from typing import Callable, List, Optional, Tuple

    from commonroad.scenario.obstacle import DynamicObstacle
    from matplotlib.axes import Axes
    from matplotlib.figure import Figure

plt.rcParams["svg.fonttype"] = "none"


def run_ignore_warning(func: "Callable", *args, **kwargs):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        func(*args, **kwargs)


def save_figure(fig: "Figure", name: str, close_after_saving: bool = True):
    path = os.path.join(Cfg["path"]["path_plot"], name)
    fig.savefig(path)
    if close_after_saving:
        plt.close(fig)


def visualize_static_scenario(
    scenario: Scenario,
    plot_limits: "Optional[List[float]]" = None,
    scale_factor: "Optional[int]" = 1,
) -> "Tuple[Figure, Axes]":
    """plot scenario with only static stuffs"""
    if plot_limits is None:
        figsize = (60, 4)
    else:
        figsize = (
            (plot_limits[1] - plot_limits[0]) / 5 * scale_factor,
            (plot_limits[3] - plot_limits[2]) / 5 * scale_factor,
        )
    fig = plt.figure(figsize=figsize)
    ax = plt.gca()
    rnd = MPRenderer(plot_limits=plot_limits, ax=ax)
    rnd.draw_params.time_begin = 0
    rnd.draw_params.time_end = 0
    rnd.draw_params.dynamic_obstacle.draw_shape = False
    rnd.draw_params.dynamic_obstacle.draw_bounding_box = False
    rnd.draw_params.dynamic_obstacle.draw_signals = False
    run_ignore_warning(
        scenario.draw,
        rnd)
    rnd.render()
    ax.set_aspect("equal")
    ax.autoscale_view()
    return fig, ax


def text_vehicle(
    ax: "Axes",
    vehicle: "DynamicObstacle",
    time_step: int,
    plot_limits: "Optional[List[float]]" = None,
    text: "Optional[str]" = None,
):
    if text is None:
        return
    state = vehicle.state_at_time(time_step)
    if state is not None:
        x = state.position[0]
        y = state.position[1]
        if plot_limits is None or Interval(plot_limits[0], plot_limits[1]).contains(x):
            ax.text(
                x,
                y,
                text,
                zorder=30,
            )


def plot_vehicle(
    ax: "Axes",
    vehicle: "DynamicObstacle",
    start_time_step: int,
    end_time_step: "Optional[int]" = None,
    plot_limits: "Optional[List[float]]" = None,
    text: "Optional[str]" = None,
    color: "Optional[str]" = "blue",
):
    if end_time_step is None:
        end_time_step = start_time_step
    rnd = MPRenderer(plot_limits=plot_limits, ax=ax)
    rnd.draw_params.time_begin = start_time_step
    rnd.draw_params.time_end = end_time_step
    rnd.draw_params.trajectory.draw_trajectory = False
    rnd.draw_params.dynamic_obstacle.vehicle_shape.occupancy.shape.opacity = 1.0
    if color == "yellow":
        rnd.draw_params.dynamic_obstacle.vehicle_shape.occupancy.facecolor = "#967000"
        rnd.draw_params.dynamic_obstacle.vehicle_shape.occupancy.edgecolor = "#967000"
        rnd.draw_params.dynamic_obstacle.vehicle_shape.direction.facecolor = "#967000"
        rnd.draw_params.dynamic_obstacle.vehicle_shape.direction.edgecolor = "#967000"
    vehicle.draw(rnd)
    rnd.render_dynamic()
    text_vehicle(ax, vehicle, start_time_step, plot_limits, text)
