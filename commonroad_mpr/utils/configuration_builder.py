import glob
import os
from typing import TYPE_CHECKING

from omegaconf import OmegaConf

if TYPE_CHECKING:
    from typing import Dict, Optional, Union

    from omegaconf import DictConfig, ListConfig


class Meta(type):
    def __getitem__(cls, arg):
        return cls.config[arg]

    def __getattr__(cls, arg):
        return cls.config[arg]

    @property
    def config(cls) -> "Union[DictConfig, ListConfig]":
        if cls._config is None:
            cls.build_configuration()
        return cls._config

    @config.setter
    def config(cls, value) -> None:
        cls._config = value

    @property
    def path_root(cls) -> str:
        if cls._path_root is None:
            cls._path_root = os.path.normpath(
                os.path.join(os.path.dirname(__file__), "../..")
            )
        return cls._path_root

    @property
    def path_config(cls) -> str:
        return os.path.join(cls.path_root, cls.folder_config)


class ConfigurationBuilder(object, metaclass=Meta):

    _path_root: str = None
    folder_config: str = "config_files"
    default_profile: str = "default"
    _config: "Union[DictConfig, ListConfig]" = None

    @classmethod
    def build_configuration(
        cls,
        config: "Optional[Dict]" = None,
        config_profile: "Optional[str]" = None,
        path_root: "Optional[str]" = None,
        folder_config: "Optional[str]" = None,
        default_profile: "Optional[str]" = None,
    ) -> None:
        """Builds configuration from default, scenario-specific, and commandline config files.

        Args:
            config (Dict): the config to be used to update the default config
            config_profile (str): the folder storing config profile to be used to update the default config
            path_root(str): root path of the package
            folder_config (str): folder storing configurations
            default_profile (str): folder storing default configurations
        """
        cls.set_paths(path_root, folder_config, default_profile)

        cls.config = cls.construct_default_configuration()

        if config_profile is not None:
            cls.update_with_profile(config_profile)

        if config is not None:
            cls.update_with_config(config)

    @classmethod
    def update_with_profile(cls, config_profile: str) -> None:
        profile = cls.construct_configuration(config_profile)
        cls._update_config(profile)

    @classmethod
    def update_with_config(cls, config: "Dict") -> None:
        if "path" in config:
            config["path"].update(cls.convert_to_absolute_paths(config["path"]))
        cls._update_config(config)

    @classmethod
    def _update_config(cls, config: "Dict | DictConfig") -> None:
        cls.config = OmegaConf.merge(cls.config, config)

    @classmethod
    def set_paths(
        cls,
        path_root: "Optional[str]" = None,
        folder_config: "Optional[str]" = None,
        default_profile: "Optional[str]" = None,
    ) -> None:
        """Sets necessary paths of the configuration builder.

        Args:
            path_root (str): root directory
            folder_config (str): folder storing configurations
            default_profile (str): folder storing default configurations
        """
        cls._path_root = path_root if path_root is not None else cls._path_root
        cls.folder_config = (
            folder_config if folder_config is not None else cls.folder_config
        )
        cls.default_profile = (
            default_profile if default_profile is not None else cls.default_profile
        )

    @classmethod
    def construct_default_configuration(cls) -> "Union[ListConfig, DictConfig]":
        """Constructs default configuration by accumulating yaml files.

        Collects all configuration files ending with '.yaml'.
        """
        return cls.construct_configuration(cls.default_profile)

    @classmethod
    def get_path_config_profile(cls, config_profile: str) -> str:
        return os.path.join(cls.path_config, config_profile)

    @classmethod
    def construct_configuration(
        cls, config_profile: str
    ) -> "Union[ListConfig, DictConfig]":
        config = OmegaConf.create()
        path_config_profile = cls.get_path_config_profile(config_profile)
        for path_file in glob.glob(path_config_profile + "/*.yaml"):
            with open(path_file, "r") as file_config:
                try:
                    config_partial = OmegaConf.load(file_config)
                    name_file, _ = os.path.splitext(os.path.basename(path_file))

                except Exception as e:
                    print(e)

                else:
                    config[name_file] = config_partial

        if "path" in config:
            OmegaConf.update(
                config, "path", cls.convert_to_absolute_paths(config["path"])
            )

        return config

    @classmethod
    def convert_to_absolute_paths(
        cls, config_path: "Union[ListConfig, DictConfig]"
    ) -> "Union[ListConfig, DictConfig]":
        """Converts relative paths to absolute paths."""
        for key, path in config_path.items():
            if os.path.isabs(path):
                continue
            absolute_path = os.path.join(cls.path_root, path)
            if not os.path.exists(absolute_path):
                os.makedirs(absolute_path)
            config_path[key] = absolute_path

        return config_path
