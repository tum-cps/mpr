import weakref
from abc import ABC
from typing import TYPE_CHECKING

import commonroad_dc.pycrccosy as pycrccosy
import numpy as np
from commonroad.scenario.trajectory import Trajectory
from commonroad.scenario.state import CustomState, InputState, InitialState, PMState
from commonroad_dc.feasibility.vehicle_dynamics import VehicleDynamics, VehicleType
from commonroad_dc.geometry.util import (
    compute_orientation_from_polyline,
    compute_pathlength_from_polyline,
)

if TYPE_CHECKING:
    from typing import Any, Dict, Iterable, List, Optional, Tuple, Union

    from commonroad.scenario.obstacle import DynamicObstacle
    from commonroad.scenario.trajectory import Trajectory
    from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
    from commonroad_mpr.prediction.ego_sampling import StateBasedSamplingResult
    from matplotlib.axes import Axes

    from .observation import World
    from .road_network import Lane, RoadNetwork


class AbstractStructure(ABC):
    __slots__ = ["parent"]
    parent: "weakref.ref"

    def set_parent(self, parent):
        self.parent = weakref.ref(parent)

    def get_parent_recursively_as_type(self, target: str) -> "AbstractStructure":
        assert (
            hasattr(self, "parent") and self.parent is not None
        ), "parent not defined."
        if type(self.parent()).__name__ == target:
            return self.parent()
        else:
            assert issubclass(
                type(self.parent()), AbstractStructure
            ), f"parent of type {target} does not exist."
            return self.parent().get_parent_recursively_as_type(target)

    def _assign_parent(self) -> None:
        pass


def convert_list_of_positions_to_cartesian_coords(
    clcs: "CurvilinearCoordinateSystem", positions: "Iterable[np.ndarray]"
) -> "List[np.ndarray]":
    # TODO multi thread
    result = []
    for s, d in positions:
        result.append(clcs.convert_to_cartesian_coords(s, d))
    return result


_vehicle_dynamics = VehicleDynamics.KS(VehicleType.BMW_320i)
_l_wb = _vehicle_dynamics.parameters.a + _vehicle_dynamics.parameters.b

rot_mat_factors = np.array([[1.0, 1.0, -1.0, -1.0], [1.0, -1.0, 1.0, -1.0]])

"""longitudinal vehicle state
s: position
v: velocity
a: acceleration
j: jerk
j_dot: first derivative of jerk
"""
_longitudinal_slots = ["s", "v", "a", "j", "j_dot"]

"""lateral vehicle state
d: lateral position
theta: orientation
kappa: curvature
kappa_dot: first derivative of curvature
kappa_ddot: second derivative of curvature
"""
_lateral_slots = ["d", "theta", "kappa", "kappa_dot", "kappa_ddot"]


def calc_s(s, w, l, theta):
    s = (
        rot_mat_factors[0] * l / 2.0 * np.cos(theta)
        - rot_mat_factors[1] * w / 2 * np.sin(theta)
        + s
    )
    return s


class AbstractVehicleState(AbstractStructure):
    def __init__(self, **kwargs):
        """Elements of state vector are determined during runtime."""
        for (field, value) in kwargs.items():
            setattr(self, field, value)

    @property
    def attributes(self) -> "List[str]":
        """Returns all dynamically set attributes of an instance of State.

        :return: subset of slots which are dynamically assigned to the object.
        """
        attributes = list()
        for slot in self.__slots__:
            if hasattr(self, slot):
                attributes.append(slot)
        return attributes

    def __str__(self):
        state = "\n"
        for attr in self.attributes:
            state += attr
            state += "= {}\n".format(self.__getattribute__(attr))
        return state


class VehicleState(AbstractVehicleState):
    __slots__ = _longitudinal_slots + _lateral_slots + ["ts"]
    S: float
    v: float
    a: float
    j: float
    j_dot: float
    d: float
    theta: float
    kappa: float
    kappa_dot: float
    kappa_ddot: float
    ts: int

    parse = {
        "v": "velocity",
        "theta": "orientation",
        "a": "acceleration",
        "j": "jerk",
        "kappa": "yaw_rate",
        "ts": "time_step",
    }

    @property
    def clcs_id(self) -> int:
        return self.parent().clcs_id

    @property
    def delta(self) -> float:
        return np.arctan(self.kappa / self.v * _l_wb)

    @property
    def v_delta(self) -> float:
        return -(_l_wb * (self.kappa * self.a - self.v * self.kappa_dot)) / (
            _l_wb**2 * self.kappa**2 + self.v**2
        )

    @property
    def rear_s(self) -> float:
        return np.min(self.calc_s())

    @property
    def front_s(self) -> float:
        return np.max(self.calc_s())

    def calc_s(self) -> np.ndarray:
        vehicle: "Vehicle" = self.get_parent_recursively_as_type("Vehicle")
        width = vehicle.width
        length = vehicle.length
        return calc_s(self.s, width, length, self.theta)

    @classmethod
    def create_from_state(cls, state: CustomState) -> "VehicleState":
        params = {}
        if hasattr(state, "position"):
            params["s"] = state.position[0]
            params["d"] = state.position[1]
        for key, value in cls.parse.items():
            if hasattr(state, value):
                params[key] = getattr(state, value)
        if hasattr(state, "steering_angle") and "kappa" not in params:
            params["kappa"] = params["v"] / _l_wb * np.tan(state.steering_angle)
        return cls(**params)

    def convert_to_commonroad_state(self) -> "CustomState":
        params = {}
        if "s" in self.attributes and "d" in self.attributes:
            params["position"] = np.array([self.s, self.d])
        for key, value in self.parse.items():
            if key in self.attributes:
                params[value] = getattr(self, key)
        params["steering_angle"] = self.delta
        params["steering_angle_speed"] = self.v_delta
        return CustomState(**params)

    def convert_to_commonroad_input_state(self) -> "InputState":
        return InputState(
            acceleration=self.a,
            steering_angle_speed=self.v_delta,
            time_step=self.ts,
        )

    def get_state_in_world_frame(self) -> "VehicleState":
        if self.clcs_id == None:
            return self
        trajectory_persp: "TrajectoryPerspective" = self.get_parent_recursively_as_type(
            "TrajectoryPerspective"
        )
        return trajectory_persp.cartesian_trajectory.state_at_time_step(self.ts)


class NotProperTrajectory(Exception):
    pass


class VehicleTrajectory(AbstractStructure):
    @classmethod
    def create_from_dynamic_obstacle(
        cls, dynamic_obstacle: "DynamicObstacle"
    ) -> "VehicleTrajectory":
        initial_time_step = dynamic_obstacle.initial_state.time_step
        final_time_step = dynamic_obstacle.prediction.final_time_step
        states = {
            time_step: VehicleState.create_from_state(
                dynamic_obstacle.state_at_time(time_step)
            )
            for time_step in range(initial_time_step, final_time_step + 1)
        }
        return cls(states)

    @classmethod
    def create_empty_trajectory(
        cls,
        initial_time_step: int,
        final_time_step: int,
        clcs_id: "Optional[int]" = None,
    ) -> "VehicleTrajectory":
        states = {
            time_step: VehicleState(ts=time_step)
            for time_step in range(initial_time_step, final_time_step + 1)
        }
        return cls(states, clcs_id)

    @classmethod
    def create_from_variable_lists(
        cls,
        variable_lists: "Dict[str, Union[List[float], int]]",
        initial_time_step: "Optional[int]" = 0,
        final_time_step: "Optional[int]" = None,
        clcs_id: "Optional[int]" = None,
    ) -> "VehicleTrajectory":
        if final_time_step is None:
            final_time_step = (
                initial_time_step + len(list(variable_lists.values())[0]) - 1
            )
        trajectory = cls.create_empty_trajectory(
            initial_time_step, final_time_step, clcs_id
        )

        for key, value in variable_lists.items():
            setattr(trajectory, key, value)
        return trajectory

    def __init__(
        self, states: "Dict[int, VehicleState]", clcs_id: "Optional[int]" = None
    ) -> None:
        self._states = states
        self.clcs_id = clcs_id
        self.feature_variables: "Dict[int, Dict[str, float]]" = {}
        self._sort_states()
        self._assign_parent()

    @property
    def time_steps(self) -> "List[int]":
        return list(self._states.keys())

    def has_time_step(self, time_step):
        return time_step in self._states

    def check_propriety(self) -> None:
        """checks whether the trajectory is properly difined.
        The following properties are checked:
        1. time_steps are intgers
        2. all time_steps between initial and final time step have states assigned.

        Returns:
            bool: is_proper
        """
        for time_step in self.time_steps:
            if not isinstance(time_step, int):
                raise NotProperTrajectory("Some time_steps are not integer.")
        if len(self._states) != self.final_time_step - self.initial_time_step + 1:
            raise NotProperTrajectory(
                "Some time steps between initial and final time step have no state assigned."
            )

    @property
    def initial_time_step(self) -> int:
        return list(self._states.keys())[0]

    @property
    def final_time_step(self) -> int:
        return list(self._states.keys())[-1]

    @property
    def states(self) -> "List[VehicleState]":
        return list(self._states.values())

    @property
    def dt(self) -> float:
        if not hasattr(self, "_dt"):
            self._dt = self.get_parent_recursively_as_type("World").dt
        return self._dt

    @property
    def polyline(self) -> np.ndarray:
        assert hasattr(self, "s") and hasattr(self, "d"), "s and d not defined."
        return np.array([self.s, self.d]).T

    @property
    def pathlength(self) -> np.ndarray:
        return compute_pathlength_from_polyline(self.polyline)

    def __getattr__(self, __name: str) -> "Any":
        if __name in VehicleState.__slots__:
            return [getattr(state, __name) for state in self.states]
        raise AttributeError

    def __setattr__(self, __name: str, __value: "Any") -> None:
        if __name in VehicleState.__slots__:
            if hasattr(__value, "__getitem__"):
                for i, state in enumerate(self.states):
                    setattr(state, __name, __value[i])
            else:
                for i, state in enumerate(self.states):
                    setattr(state, __name, __value)
            return
        super().__setattr__(__name, __value)

    def state_at_time_step(self, time_step: int) -> VehicleState:
        return self._states[time_step]

    def set_state_at_time_step(self, time_step: int, state: "VehicleState") -> None:
        self._states[time_step] = state
        self._sort_states()

    def assign_feature_variable(
        self, time_step: int, feature_variable: "Dict[str, float]"
    ) -> None:
        self.feature_variables[time_step] = feature_variable

    def has_feature_variable_assignment(self, time_step: int) -> bool:
        return time_step in self.feature_variables

    def get_feature_variable(self, time_step: int) -> "Dict[str, float]":
        return self.feature_variables[time_step]

    def append_from_another(
        self, another: "VehicleTrajectory", start_time_step: int, end_time_step: int
    ) -> None:
        for time_step in range(start_time_step, end_time_step + 1):
            if not another.has_time_step(time_step):
                continue
            another_state = another.state_at_time_step(time_step)
            self.set_state_at_time_step(time_step, another_state)
        self.check_propriety()

    def crop(self, start_time_step: int, end_time_step: int) -> None:
        time_steps_pre = self.time_steps
        for time_step in time_steps_pre:
            if time_step < start_time_step or time_step > end_time_step:
                del self._states[time_step]

    def variable_at_time_step(self, name: str, time_step: int) -> float:
        return getattr(self.state_at_time_step(time_step), name)

    def variable_at_time_range(
        self,
        name: str,
        start_time_step: "Optional[int]" = None,
        end_time_step: "Optional[int]" = None,
    ) -> "List[float]":
        var = getattr(self, name)
        start_idx = (
            0 if start_time_step is None else start_time_step - self.initial_time_step
        )
        end_idx = len(var) - (
            0 if end_time_step is None else self.final_time_step - end_time_step
        )
        return var[start_idx:end_idx]

    def convert_to_commonroad_trajectory(self) -> Trajectory:
        state_list = [state.convert_to_commonroad_state() for state in self.states]
        return Trajectory(self.initial_time_step, state_list)

    def convert_to_commonroad_input_trajectory(self) -> Trajectory:
        input_list = [
            state.convert_to_commonroad_input_state() for state in self.states
        ]
        return Trajectory(self.initial_time_step, input_list)

    def compute_v_from_position(self) -> None:
        self.v = np.gradient(self.pathlength, self.dt)

    def compute_a_from_v(self) -> None:
        assert hasattr(self, "v"), "v not defined."
        self.a = np.gradient(self.v, self.dt)

    def correct_a_from_v(self) -> None:
        assert hasattr(self, "v"), "v not defined."
        a_from_v = np.gradient(self.v, self.dt)
        a_origin = np.array(self.a)
        mask = (a_from_v * a_origin) < 0
        a_origin[mask] *= -1
        self.a = a_origin

    def compute_j_from_a(self) -> None:
        assert hasattr(self, "a"), "a not defined."
        self.j = np.gradient(self.a, self.dt)

    def compute_j_dot_from_j(self) -> None:
        assert hasattr(self, "j"), "j not defined."
        self.j_dot = np.gradient(self.j, self.dt)

    def compute_theta_from_position(self) -> None:
        self.theta = compute_orientation_from_polyline(self.polyline)

    def compute_kappa_from_position(self) -> None:
        self.kappa = pycrccosy.Util.compute_curvature(self.polyline)

    def compute_kappa_dot_from_kappa(self) -> None:
        assert hasattr(self, "kappa"), "kappa not defined."
        self.kappa_dot = np.gradient(self.kappa, self.pathlength)

    def compute_kappa_ddot_from_kappa_dot(self) -> None:
        assert hasattr(self, "kappa_dot"), "kappa dot not defined."
        self.kappa_ddot = np.gradient(self.kappa_dot, self.pathlength)

    def compute_missing_variables(self) -> None:
        if not hasattr(self, "v"):
            self.compute_v_from_position()
        if not hasattr(self, "a"):
            self.compute_a_from_v()
        else:
            self.correct_a_from_v()
        if not hasattr(self, "j"):
            self.compute_j_from_a()
        if not hasattr(self, "j_dot"):
            self.compute_j_dot_from_j()
        if not hasattr(self, "theta"):
            self.compute_theta_from_position()
        if not hasattr(self, "kappa"):
            self.compute_kappa_from_position()
        if not hasattr(self, "kappa_dot"):
            self.compute_kappa_dot_from_kappa()
        if not hasattr(self, "kappa_ddot"):
            self.compute_kappa_ddot_from_kappa_dot()

    def _sort_states(self) -> None:
        self._states = dict(sorted(self._states.items()))

    def _assign_parent(self) -> None:
        for state in self.states:
            state.set_parent(self)


class TrajectoryPerspective(AbstractStructure):
    class CoordinateTransformer:
        @staticmethod
        def transfer_state_to_clcs(
            state: "VehicleState",
            lane: "Lane",
            clcs: "Optional[CurvilinearCoordinateSystem]" = None,
        ) -> "VehicleState":
            if clcs is None:
                clcs = lane.clcs
            s, d = clcs.convert_to_curvilinear_coords(state.s, state.d)
            theta = state.theta - lane.orientation(s)
            return VehicleState(
                s=s,
                d=d,
                v=state.v,
                a=state.a,
                j=state.j,
                theta=theta,
                ts=state.ts,
            )

        @classmethod
        def transfer_list_of_positions_in_world_frame(
            cls, clcs: "CurvilinearCoordinateSystem", s: np.ndarray, d: np.ndarray
        ) -> "Tuple[np.ndarray, np.ndarray]":
            result = convert_list_of_positions_to_cartesian_coords(clcs, zip(s, d))
            result = np.array(result)
            return result[:, 0], result[:, 1]

        @staticmethod
        def transfer_position_in_world_frame(
            clcs: "CurvilinearCoordinateSystem", s: float, d: float
        ) -> "Tuple[float, float]":
            pos: np.ndarray = clcs.convert_to_cartesian_coords(s, d)
            return pos[0], pos[1]

        @classmethod
        def create_trajectory_perspective_from_functions(
            cls,
            long_fun: np.polynomial.Polynomial,
            lat_fun: np.polynomial.Polynomial,
            vehicle: "Vehicle",
            initial_time_step: int,
            num_ts: int,
        ) -> "TrajectoryPerspective":
            # time
            dt: float = vehicle.parent().dt
            t = np.arange(0, dt * (num_ts + 1), dt)

            # long
            s = long_fun(t)
            s_d = long_fun.deriv(1)(t)
            assert (
                s_d > 0.001
            ).all(), "The vehicle is at low speed, which is not supported."
            s_dd = long_fun.deriv(2)(t)

            # lat
            d = lat_fun(t)
            d_d = lat_fun.deriv(1)(t)
            d_dd = lat_fun.deriv(2)(t)
            d_p = d_d / s_d
            d_pp = (d_dd - d_p * s_dd) / (s_d) ** 2

            # ref path
            ref_path = vehicle.trajectory_persp.get_reference_lane(initial_time_step)
            theta_r = ref_path.orientation(s)
            kappa_r = ref_path.curvature(s)
            kappa_r_p = ref_path.curvature_prime(s)

            # position
            x, y = cls.transfer_list_of_positions_in_world_frame(ref_path.clcs, s, d)

            # helper variable
            one_krd = 1 - kappa_r * d
            krpd_krdp = kappa_r_p * d + kappa_r * d_p

            # orientation
            delta_theta = np.arctan2(d_p, one_krd)
            theta = delta_theta + theta_r

            # helper variable
            cos_dtheta = np.cos(delta_theta)
            tan_dtheta = np.tan(delta_theta)

            # velocity
            v = s_d * one_krd / cos_dtheta

            # curvature
            kappa = cls._get_kappa(
                d_pp, kappa_r, one_krd, krpd_krdp, cos_dtheta, tan_dtheta
            )

            # acceleration
            a = cls._get_acceleration(
                s_d,
                s_dd,
                d_p,
                d_pp,
                kappa_r,
                one_krd,
                krpd_krdp,
                cos_dtheta,
                tan_dtheta,
            )

            trajectory = VehicleTrajectory.create_from_variable_lists(
                {
                    "s": x,
                    "d": y,
                    "v": v,
                    "a": a,
                    "theta": theta,
                    "kappa": kappa,
                },
                initial_time_step,
            )
            trajectory_clcs = VehicleTrajectory.create_from_variable_lists(
                {
                    "s": s,
                    "d": d,
                    "v": v,
                    "a": a,
                    "theta": delta_theta,
                },
                initial_time_step,
                clcs_id=ref_path.id,
            )
            trajectory_persp = TrajectoryPerspective(
                {None: trajectory, ref_path.id: trajectory_clcs}
            )
            trajectory_persp.set_parent(vehicle)
            return trajectory_persp

        @staticmethod
        def _get_kappa(
            d_pp: np.ndarray,
            kappa_r: np.ndarray,
            one_krd: np.ndarray,
            krpd_krdp: np.ndarray,
            cos_dtheta: np.ndarray,
            tan_dtheta: np.ndarray,
        ) -> np.ndarray:
            return (
                ((d_pp + krpd_krdp * tan_dtheta) * cos_dtheta**2 / one_krd + kappa_r)
                * cos_dtheta
                / one_krd
            )

        @staticmethod
        def _get_acceleration(
            s_d: np.ndarray,
            s_dd: np.ndarray,
            d_p: np.ndarray,
            d_pp: np.ndarray,
            kappa_r: np.ndarray,
            one_krd: np.ndarray,
            krpd_krdp: np.ndarray,
            cos_dtheta: np.ndarray,
            tan_dtheta: np.ndarray,
        ) -> np.ndarray:
            kappa_gl = (d_pp + kappa_r * d_p * tan_dtheta) * cos_dtheta * (
                cos_dtheta / one_krd
            ) ** 2 + (cos_dtheta / one_krd) * kappa_r
            delta_theta_p = kappa_gl * one_krd / cos_dtheta - kappa_r
            return s_dd * one_krd / cos_dtheta + s_d**2 / cos_dtheta * (
                one_krd * tan_dtheta * delta_theta_p - krpd_krdp
            )

        @classmethod
        def get_long_lat_state(
            cls, state_in_clcs: "VehicleState", lane: "Lane"
        ) -> "Tuple[List[float], List[float]]":
            state = state_in_clcs.get_state_in_world_frame()

            s, d = state_in_clcs.s, state_in_clcs.d

            kappa_r = lane.curvature(s)
            kappa_r_p = lane.curvature_prime(s)

            one_krd = 1 - kappa_r * d
            cos_dtheta = np.cos(state_in_clcs.theta)
            tan_dtheta = np.tan(state_in_clcs.theta)

            d_p = one_krd * tan_dtheta
            krpd_krdp = kappa_r_p * d + kappa_r * d_p
            d_pp = -krpd_krdp * tan_dtheta + (one_krd / (cos_dtheta**2)) * (
                state.kappa * one_krd / cos_dtheta - kappa_r
            )

            s_d = state_in_clcs.v * cos_dtheta / one_krd
            s_dd = state_in_clcs.a
            s_dd -= (s_d**2 / cos_dtheta) * (
                one_krd * tan_dtheta * (state.kappa * one_krd / cos_dtheta - kappa_r)
                - krpd_krdp
            )
            s_dd /= one_krd / cos_dtheta

            d_d = state_in_clcs.v * np.sin(state_in_clcs.theta)
            d_dd = s_dd * d_p + s_d**2 * d_pp

            long_left = [s, s_d, s_dd]
            lat_left = [d, d_d, d_dd]
            return long_left, lat_left

    @classmethod
    def create_from_functions(
        cls,
        long_fun: np.polynomial.Polynomial,
        lat_fun: np.polynomial.Polynomial,
        vehicle: "Vehicle",
        initial_time_step: int,
        num_ts: int,
    ) -> "TrajectoryPerspective":
        trajectory_persp = (
            cls.CoordinateTransformer.create_trajectory_perspective_from_functions(
                long_fun,
                lat_fun,
                vehicle,
                initial_time_step,
                num_ts + 2,
            )
        )
        trajectory_persp.append_from_another(
            vehicle.trajectory_persp, initial_time_step - 2, initial_time_step - 1
        )
        for trajectory in trajectory_persp.trajectories:
            trajectory.compute_missing_variables()
        trajectory_persp.crop(initial_time_step, initial_time_step + num_ts)
        return trajectory_persp

    @classmethod
    def create_from_dynamic_obstacle(
        cls, dynamic_obstacle: "DynamicObstacle"
    ) -> "TrajectoryPerspective":
        trajectory = VehicleTrajectory.create_from_dynamic_obstacle(dynamic_obstacle)
        return cls({None: trajectory})

    def __init__(self, trajectories: "Dict[Optional[int], VehicleTrajectory]") -> None:
        self._trajectories = trajectories
        self.reference_lane_assignment: "Dict[int, int]" = {}
        self.occupied_lanes_assignment: "Dict[int, List[int]]" = {}
        self._assign_parent()

    @property
    def trajectories(self) -> "List[VehicleTrajectory]":
        return list(self._trajectories.values())

    @property
    def clcs_ids(self) -> "List[Optional[int]]":
        return list(self._trajectories.keys())

    @property
    def initial_time_step(self) -> int:
        return self.cartesian_trajectory.initial_time_step

    @property
    def final_time_step(self) -> int:
        return self.cartesian_trajectory.final_time_step

    @property
    def cartesian_trajectory(self) -> "VehicleTrajectory":
        return self._trajectories[None]

    def covers_time_range(
        self,
        start_time_step: int,
        end_time_step: "Optional[int]" = None,
        num_ts: "Optional[int]" = None,
    ) -> bool:
        if end_time_step is None:
            if num_ts is not None:
                end_time_step = start_time_step + num_ts
            else:
                end_time_step = start_time_step
        return (
            self.initial_time_step <= start_time_step
            and self.final_time_step >= end_time_step
        )

    def has_clcs(self, id: "Optional[int]" = None) -> bool:
        return id in self.clcs_ids

    def trajectory_by_clcs_id(self, id: "Optional[int]" = None) -> "VehicleTrajectory":
        if not self.has_clcs(id):
            self.create_perspective(id)
        return self._trajectories[id]

    def create_perspective(
        self,
        id: int,
        initial_time_step: "Optional[int]" = None,
        final_time_step: "Optional[int]" = None,
    ) -> None:
        if initial_time_step is None:
            initial_time_step = self.initial_time_step
        if final_time_step is None:
            final_time_step = self.final_time_step
        world_state: World = self.get_parent_recursively_as_type("World")
        lane = world_state.road_network.lane_by_id(id)
        clcs = lane.clcs

        states: "Dict[int, VehicleState]" = {}
        for time_step in range(initial_time_step, final_time_step + 1):
            state = self.cartesian_trajectory.state_at_time_step(time_step)
            new_state = self.CoordinateTransformer.transfer_state_to_clcs(
                state, lane, clcs
            )
            states[time_step] = new_state

        new_trajectory = VehicleTrajectory(states, clcs_id=id)
        self._assign_parent(new_trajectory)
        new_trajectory.compute_missing_variables()
        self._trajectories[id] = new_trajectory

    def assign_occupied_lanes(self, time_step: int, lanes: "List[Lane]") -> None:
        self.occupied_lanes_assignment[time_step] = [l.id for l in lanes]

    def has_occupied_lanes_assignment(self, time_step: int) -> bool:
        return time_step in self.occupied_lanes_assignment

    def get_occupied_lanes(self, time_step: int) -> "List[Lane]":
        road_network: "RoadNetwork" = self.get_parent_recursively_as_type(
            "World"
        ).road_network
        if not self.has_occupied_lanes_assignment(time_step):
            state = self.cartesian_trajectory.state_at_time_step(time_step)
            lanes = road_network.lanes_by_vehicle_state(state)
            self.assign_occupied_lanes(time_step, lanes)
        return [
            road_network.lane_by_id(id)
            for id in self.occupied_lanes_assignment[time_step]
        ]

    def assign_reference_lane(self, time_step: int, lane: "Lane") -> None:
        self.reference_lane_assignment[time_step] = lane.id

    def has_reference_lane_assignment(self, time_step: int) -> bool:
        return time_step in self.reference_lane_assignment

    def get_reference_lane(self, time_step: int) -> "Lane":
        road_network: "RoadNetwork" = self.get_parent_recursively_as_type(
            "World"
        ).road_network
        if not self.has_reference_lane_assignment(time_step):
            state = self.cartesian_trajectory.state_at_time_step(time_step)
            ref_lane = road_network.lane_by_vehicle_state(state)
            self.assign_reference_lane(time_step, ref_lane)
        return road_network.lane_by_id(self.reference_lane_assignment[time_step])

    def get_long_lat_state(
        self, time_step: int, lane: "Optional[Lane]" = None
    ) -> "Tuple[List[float], List[float]]":
        if lane is None:
            lane = self.get_reference_lane(time_step)
        state_in_clcs = self.trajectory_by_clcs_id(lane.id).state_at_time_step(
            time_step
        )
        return self.CoordinateTransformer.get_long_lat_state(state_in_clcs, lane)

    def append_from_another(
        self, another: "TrajectoryPerspective", start_time_step: int, end_time_step: int
    ) -> None:
        for clcs_id, trajectory in self._trajectories.items():
            another_trajectory = another.trajectory_by_clcs_id(clcs_id)
            trajectory.append_from_another(
                another_trajectory, start_time_step, end_time_step
            )

    def crop(self, start_time_step: int, end_time_step: int) -> None:
        for trajectory in self.trajectories:
            trajectory.crop(start_time_step, end_time_step)

    def convert_to_commonroad_trajectory(self) -> "Trajectory":
        return self.cartesian_trajectory.convert_to_commonroad_trajectory()

    def convert_to_commonroad_input_trajectory(self) -> "Trajectory":
        return self.cartesian_trajectory.convert_to_commonroad_input_trajectory()

    def _assign_parent(self, target: "Optional[VehicleTrajectory]" = None):
        if target is None:
            for trajectory in self.trajectories:
                trajectory.set_parent(self)
        else:
            target.set_parent(self)

    def plot(
        self,
        ax: "Axes",
        start_time_step: "Optional[int]" = None,
        end_time_step: "Optional[int]" = None,
        **kwargs,
    ):
        trajectory = self.cartesian_trajectory
        s = trajectory.variable_at_time_range("s", start_time_step, end_time_step)
        d = trajectory.variable_at_time_range("d", start_time_step, end_time_step)
        ax.plot(s, d, zorder=29, **kwargs)


class Vehicle(AbstractStructure):
    vehicle_dynamics = VehicleDynamics.KS(VehicleType.BMW_320i)

    @classmethod
    def create_from_dynamic_obstacle(
        cls, dynamic_obstacle: "DynamicObstacle"
    ) -> "Vehicle":
        trajectory_persp = TrajectoryPerspective.create_from_dynamic_obstacle(
            dynamic_obstacle
        )
        return cls(
            dynamic_obstacle.obstacle_id,
            trajectory_persp,
            dynamic_obstacle.obstacle_shape.length,
            dynamic_obstacle.obstacle_shape.width,
        )

    def __init__(
        self,
        id: int,
        trajectory_persp: TrajectoryPerspective,
        length: "Optional[float]" = None,
        width: "Optional[float]" = None,
    ) -> None:
        self.id = id
        self.trajectory_persp = trajectory_persp
        self.future_state_sampling_cache = {}
        if length is not None:
            self._length = length
        if width is not None:
            self._width = width
        self._assign_parent()

    @property
    def width(self) -> float:
        if not hasattr(self, "_width"):
            world_state: "World" = self.parent()
            self._width = world_state.scenario.obstacle_by_id(
                self.id
            ).obstacle_shape.width
        return self._width

    @property
    def length(self) -> float:
        if not hasattr(self, "_length"):
            world_state: "World" = self.parent()
            self._length = world_state.scenario.obstacle_by_id(
                self.id
            ).obstacle_shape.length
        return self._length

    @property
    def initial_time_step(self) -> int:
        return self.trajectory_persp.initial_time_step

    @property
    def final_time_step(self) -> int:
        return self.trajectory_persp.final_time_step

    def append_cache(self, key: str, value: "StateBasedSamplingResult") -> None:
        # self._assign_parent(value) # already assigned when generating
        self.future_state_sampling_cache[key] = value

    def has_cache(self, key: str) -> bool:
        return key in self.future_state_sampling_cache

    def get_cache(self, key: str) -> "StateBasedSamplingResult":
        return self.future_state_sampling_cache[key]

    def _assign_parent(
        self, target: "Optional[StateBasedSamplingResult]" = None
    ) -> None:
        if target is None:
            self.trajectory_persp.set_parent(self)
        else:
            for trajectory_persp in target.trajectory_persps:
                trajectory_persp.set_parent(self)
