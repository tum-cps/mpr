import enum
from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

import numpy as np
from commonroad.scenario.traffic_sign import SupportedTrafficSignCountry
from commonroad.scenario.traffic_sign_interpreter import TrafficSigInterpreter

from .observation import parse

if TYPE_CHECKING:
    from typing import List

    from .observation import World
    from .vehicle import TrajectoryPerspective, VehicleState


class AbstractPredicate(ABC):
    arity: int = 0
    name: str = ""

    @classmethod
    @abstractmethod
    def evaluate(cls, **kwargs) -> bool:
        """evaluate predicate

        Args:
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            bool: boolean evaluation of predicate
        """
        pass


class AbstractUnaryPredicate(AbstractPredicate, ABC):
    arity = 1


class AbstractBinaryPredicate(AbstractPredicate, ABC):
    arity = 2


class AbstractSpeedLimit(AbstractUnaryPredicate):
    country = SupportedTrafficSignCountry("DEU")

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        world_state, vehicles, states = parse(**kwargs)
        ego_state = states[0]
        speed_limit = cls.get_speed_limit(
            world_state=world_state, vehicles=vehicles, states=states
        )
        return ego_state.v <= speed_limit

    @classmethod
    @abstractmethod
    def get_speed_limit(cls, **kwargs) -> float:
        pass


class AbstractBrakesAbruptly(ABC):
    a_abrupt = -2


class InSameLane(AbstractBinaryPredicate):
    name = "in_same_lane"

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        _, _, states = parse(**kwargs)
        ego_state, other_state = states[:2]
        ego_traj: TrajectoryPerspective = ego_state.get_parent_recursively_as_type(
            "TrajectoryPerspective"
        )
        other_traj: TrajectoryPerspective = (
            other_state.get_parent_recursively_as_type("TrajectoryPerspective")
        )
        ego_lanes = ego_traj.get_occupied_lanes(ego_state.ts)
        other_lanes = other_traj.get_occupied_lanes(ego_state.ts)
        ego_lane_ids = set(lane.id for lane in ego_lanes if hasattr(lane, "id"))
        other_lane_ids = set(lane.id for lane in other_lanes if hasattr(lane, "id"))
        return bool(ego_lane_ids & other_lane_ids)


class SingleLane(AbstractUnaryPredicate):
    name = "single_lane"

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        _, _, states = parse(**kwargs)
        ego_state = states[0]
        ego_traj: "TrajectoryPerspective" = ego_state.get_parent_recursively_as_type(
            "TrajectoryPerspective"
        )
        ego_lanes = ego_traj.get_occupied_lanes(ego_state.ts)
        return len(ego_lanes) == 1


class KeepsSafeDistancePrec(AbstractBinaryPredicate):
    name = "keeps_safe_distance_prec"

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        _, vehicles, states = parse(**kwargs)
        follow_state, lead_state = states[:2]
        # follow_vehicle, lead_vehicle = vehicles[:2]
        d_safe = cls.calculate_safe_distance(
            follow_state.v,
            lead_state.v,
            -10.5,  # hard-coded from Sebastian's paper
            -10,  # hard-coded from Sebastian's paper
        )
        return lead_state.rear_s - follow_state.front_s >= d_safe

    @classmethod
    def calculate_safe_distance(
        cls, v_follow, v_lead, a_min_lead, a_min_follow, t_react_follow=0.4
    ):
        d_safe = (
            (v_lead**2) / (-2 * np.abs(a_min_lead))
            - (v_follow**2) / (-2 * np.abs(a_min_follow))
            + v_follow * t_react_follow
        )

        return d_safe


class InFrontOf(AbstractBinaryPredicate):
    name = "in_front_of"

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        _, _, states = parse(**kwargs)
        follow_state, lead_state = states[:2]
        return lead_state.rear_s > follow_state.front_s


class KeepsLaneSpeedLimit(AbstractSpeedLimit):
    name = "keeps_lane_speed_limit"

    @classmethod
    def get_speed_limit(
        cls, world_state: "World", states: "List[VehicleState]", **kwargs
    ) -> float:
        ego_state = states[0]
        ts_interpreter = TrafficSigInterpreter(
            cls.country, world_state.scenario.lanelet_network
        )
        ego_traj: "TrajectoryPerspective" = ego_state.get_parent_recursively_as_type(
            "TrajectoryPerspective"
        )
        lanes = ego_traj.get_occupied_lanes(ego_state.ts)
        lanelet_ids = [
            id
            for lane in lanes
            if hasattr(lane, "segment_ids")
            for id in lane.segment_ids
        ]
        lane_speed_limit = ts_interpreter.speed_limit(frozenset(lanelet_ids))
        if lane_speed_limit is None:
            lane_speed_limit = np.inf
        return lane_speed_limit


class KeepsFovSpeedLimit(AbstractSpeedLimit):
    name = "keeps_fov_speed_limit"

    @classmethod
    def get_speed_limit(cls, **kwargs) -> float:
        # TODO implement referencing stl_crmonitor: calc_v_max_fov
        return 50


class KeepsBrakeSpeedLimit(AbstractSpeedLimit):
    name = "keeps_brake_speed_limit"

    @classmethod
    def get_speed_limit(cls, **kwargs) -> float:
        # TODO implement referencing stl_crmonitor: calc_v_max_fov
        return 43


class KeepsTypeSpeedLimit(AbstractSpeedLimit):
    name = "keeps_type_speed_limit"

    @classmethod
    def get_speed_limit(cls, **kwargs) -> float:
        # TODO implement regarding the vehicle type
        # since we filter out non-car vehicles, the limit is always inf
        return np.inf


class CutIn(AbstractUnaryPredicate):
    name = "cut_in"
    eps = 1e-5

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        world_state, vehicles, states = parse(**kwargs)
        if SingleLane.evaluate(
            world_state=world_state, vehicles=vehicles, states=states
        ):
            return False
        if not InSameLane.evaluate(
            world_state=world_state, vehicles=vehicles, states=states
        ):
            return False
        ego_state, other_state = states[:2]
        return (other_state.d > ego_state.d and ego_state.theta > cls.eps) or (
            other_state.d < ego_state.d and ego_state.theta < -cls.eps
        )


class BrakesAbruptly(AbstractBrakesAbruptly, AbstractUnaryPredicate):
    name = "brakes_abruptly"

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        _, _, states = parse(**kwargs)
        ego_state = states[0]
        return ego_state.a < cls.a_abrupt


class BrakesAbruptlyRelative(AbstractBrakesAbruptly, AbstractBinaryPredicate):
    name = "rel_brakes_abruptly"

    @classmethod
    def evaluate(
        cls,
        **kwargs,
    ) -> bool:
        _, _, states = parse(**kwargs)
        ego_state, other_state = states[:2]
        return (ego_state.a - other_state.a) < cls.a_abrupt


@enum.unique
class Predicates(enum.Enum):
    CUT_IN = CutIn
    IN_FRONT_OF = InFrontOf
    IN_SAME_LANE = InSameLane
    KEEPS_LANE_SPEED_LIMIT = KeepsLaneSpeedLimit
    KEEPS_FOV_SPEED_LIMIT = KeepsFovSpeedLimit
    KEEPS_TYPE_SPEED_LIMIT = KeepsTypeSpeedLimit
    KEEPS_BRAKE_SPEED_LIMIT = KeepsBrakeSpeedLimit
    BRAKES_ABRUPTLY = BrakesAbruptly
    REL_BRAKES_ABRUPTLY = BrakesAbruptlyRelative
    KEEPS_SAFE_DISTANCE_PREC = KeepsSafeDistancePrec
    SINGLE_LANE = SingleLane
    

class PredicateEvaluator:
    def __init__(self, predicate_names: "List[str]") -> None:
        self.predicates: "List[AbstractPredicate]" = [
            Predicates[name.upper()].value for name in predicate_names
        ]

    @property
    def predicate_names(self) -> "List[str]":
        return [predicate.name for predicate in self.predicates]

    @property
    def arity(self) -> int:
        return max(predicate.arity for predicate in self.predicates)

    def evaluate(self, **kwargs) -> "np.ndarray":
        """boolean evaluate predicate

        Args:
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            np.ndarray: boolean evaluation of predicates.

        """
        world_state, vehicles, states = parse(**kwargs)

        boolean = np.zeros_like(self.predicates)
        for i, predicate in enumerate(self.predicates):
            try:
                if predicate.evaluate(
                    world_state=world_state, vehicles=vehicles, states=states
                ):
                    boolean[i] = 1
            except:
                boolean[i] = -1  # error in evaluation

        return boolean

    def characteristic_function(self, **kwargs) -> "np.ndarray":
        """compute characteristic function of the predicates

        Args:
            world_state (WroldState):
            vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
            time_step (int):
            scenario (Scenario): If world_state is missing, generate from it
            vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
            states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
                are missing, get them from it.
        Returns:
            np.ndarray: result of the characteristic functions.

        """
        res = self.evaluate(**kwargs)
        res[np.logical_not(res).astype("bool")] = -1
        return res
