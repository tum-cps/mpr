from abc import abstractmethod
from typing import TYPE_CHECKING

import commonroad_dc.pycrccosy as pycrccosy
import numpy as np
from commonroad_dc.geometry.util import (
    chaikins_corner_cutting,
    compute_orientation_from_polyline,
    compute_pathlength_from_polyline,
    resample_polyline,
)
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg

from .vehicle import AbstractStructure, convert_list_of_positions_to_cartesian_coords

if TYPE_CHECKING:
    from typing import Dict, Iterable, List, Optional, Tuple, Union

    from commonroad.geometry.shape import Shape
    from commonroad.scenario.lanelet import Lanelet
    from commonroad.scenario.scenario import Scenario

    from .observation import World
    from .vehicle import VehicleState


class AbstractLane(AbstractStructure):
    def __init__(self):
        self.clcs_right = None
        self.clcs_left = None

    @classmethod
    @abstractmethod
    def contains_point(self, x: float, y: float) -> bool:
        pass

    def contains_point_left(self, x: float, y: float) -> bool:
        return self.clcs_left.convert_to_curvilinear_coords(x, y)[1] <= 0

    def contains_point_right(self, x: float, y: float) -> bool:
        return self.clcs_right.convert_to_curvilinear_coords(x, y)[1] > 0

    def contains_at_least_one_point(
        self, points: np.ndarray, direction: "Optional[str]" = None
    ) -> bool:
        if direction is None:
            suffix = ""
        else:
            suffix = "_" + direction

        for x, y in points:
            if getattr(self, "contains_point" + suffix)(x, y):
                return True
        return False

    def distance_to_left(self, x: float, y: float) -> float:
        return -self.clcs_left.convert_to_curvilinear_coords(x, y)[1]

    def distance_to_right(self, x: float, y: float) -> float:
        return self.clcs_right.convert_to_curvilinear_coords(x, y)[1]

    def min_max_distance_to_left(self, points: np.ndarray) -> "Tuple[float, float]":
        minimum = np.inf
        maximum = -np.inf
        for x, y in points:
            distance = self.distance_to_left(x, y)
            minimum = min(minimum, distance)
            maximum = max(maximum, distance)
        return minimum, maximum

    def min_max_distance_to_right(self, points: np.ndarray) -> "Tuple[float, float]":
        minimum = np.inf
        maximum = -np.inf
        for x, y in points:
            distance = self.distance_to_right(x, y)
            minimum = min(minimum, distance)
            maximum = max(maximum, distance)
        return minimum, maximum


class Lane(AbstractLane):
    @classmethod
    def create_from_lanelet(
        cls,
        id: int,
        lanelet: "Lanelet",
        merge_jobs: "Optional[List[int]]" = None,
    ) -> "Lane":
        clcs_left = cls._create_curvilinear_coordinate_system_from_reference(
            lanelet.left_vertices
        )
        clcs_right = cls._create_curvilinear_coordinate_system_from_reference(
            lanelet.right_vertices
        )
        clcs = cls._create_curvilinear_coordinate_system_from_reference(
            lanelet.center_vertices
        )
        (
            orientation,
            curvature,
            curvature_prime,
            path_length,
            width,
        ) = cls._compute_lanelet_properties(lanelet)
        if merge_jobs is None:
            merge_jobs = [id]
        return cls(
            id,
            clcs,
            clcs_left,
            clcs_right,
            orientation,
            curvature,
            curvature_prime,
            path_length,
            width,
            merge_jobs,
            lanelet.adj_left,
            lanelet.adj_right,
        )

    @classmethod
    def _compute_lanelet_properties(cls, lanelet: "Lanelet"):
        orientation = compute_orientation_from_polyline(lanelet.center_vertices)
        curvature = pycrccosy.Util.compute_curvature(lanelet.center_vertices)
        path_length = compute_pathlength_from_polyline(lanelet.center_vertices)
        width = cls._compute_width_from_lanalet_boundary(
            lanelet.left_vertices, lanelet.right_vertices
        )
        curvature_prime = np.diff(curvature) / np.diff(path_length)
        curvature_prime = np.concatenate((curvature_prime, np.array([0])))
        return orientation, curvature, curvature_prime, path_length, width

    @classmethod
    def create_phantom_from_lanes(
        cls, left_lane: "Lane", right_lane: "Lane", scenario: "Scenario"
    ) -> "Tuple[Lane, Lane]":
        # left phantom lane
        id = scenario.generate_object_id()
        ref_path = left_lane.clcs_left.reference_path_original()
        ref_path_zero = (
            left_lane.clcs_left.convert_list_of_points_to_curvilinear_coords(
                ref_path, Cfg["general"]["num_threads"]
            )
        )
        phantom_width = Cfg["common"]["lane"]["phantom_lane_width"]
        ref_path_mid = convert_list_of_positions_to_cartesian_coords(
            left_lane.clcs_left,
            cls._offset_path(ref_path_zero, np.array([0, -phantom_width / 2])),
        )
        clcs = CurvilinearCoordinateSystem(
            ref_path_mid,
            Cfg["common"]["lane"].get("lateral_projection_domain_limit"),
            Cfg["common"]["lane"].get("lateral_eps"),
        )
        ref_path_bound = convert_list_of_positions_to_cartesian_coords(
            left_lane.clcs_left,
            cls._offset_path(ref_path_zero, np.array([0, -phantom_width])),
        )
        clcs_left = CurvilinearCoordinateSystem(
            ref_path_bound,
            Cfg["common"]["lane"].get("lateral_projection_domain_limit"),
            Cfg["common"]["lane"].get("lateral_eps"),
        )
        (
            orientation,
            curvature,
            curvature_prime,
            path_length,
            width,
        ) = cls._compute_phantom_properties(ref_path_mid)
        left_phantom_lane = cls(
            id,
            clcs,
            clcs_left,
            left_lane.clcs_left,
            orientation,
            curvature,
            curvature_prime,
            path_length,
            width,
            adj_right=left_lane.id,
            is_real=False,
        )

        # right phantom lane
        id = scenario.generate_object_id()
        ref_path = right_lane.clcs_right.reference_path_original()
        ref_path_zero = (
            left_lane.clcs_left.convert_list_of_points_to_curvilinear_coords(
                ref_path, Cfg["general"]["num_threads"]
            )
        )
        ref_path_mid = convert_list_of_positions_to_cartesian_coords(
            left_lane.clcs_left,
            cls._offset_path(ref_path_zero, np.array([0, phantom_width / 2])),
        )
        clcs = CurvilinearCoordinateSystem(
            ref_path_mid,
            Cfg["common"]["lane"].get("lateral_projection_domain_limit"),
            Cfg["common"]["lane"].get("lateral_eps"),
        )
        ref_path_bound = convert_list_of_positions_to_cartesian_coords(
            left_lane.clcs_left,
            cls._offset_path(ref_path_zero, np.array([0, phantom_width])),
        )
        clcs_right = CurvilinearCoordinateSystem(
            ref_path_bound,
            Cfg["common"]["lane"].get("lateral_projection_domain_limit"),
            Cfg["common"]["lane"].get("lateral_eps"),
        )
        (
            orientation,
            curvature,
            curvature_prime,
            path_length,
            width,
        ) = cls._compute_phantom_properties(ref_path_mid)
        right_phantom_lane = cls(
            id,
            clcs,
            right_lane.clcs_right,
            clcs_right,
            orientation,
            curvature,
            curvature_prime,
            path_length,
            width,
            adj_left=right_lane.id,
            is_real=False,
        )
        return left_phantom_lane, right_phantom_lane

    @staticmethod
    def _compute_phantom_properties(ref_path_mid: "List[np.ndarray]"):
        center_vertices = np.array(ref_path_mid)
        orientation = compute_orientation_from_polyline(center_vertices)
        curvature = pycrccosy.Util.compute_curvature(center_vertices)
        path_length = compute_pathlength_from_polyline(center_vertices)
        width = np.zeros_like(path_length) + Cfg["common"]["lane"]["phantom_lane_width"]
        curvature_prime = np.diff(curvature) / np.diff(path_length)
        curvature_prime = np.concatenate((curvature_prime, np.array([0])))
        return orientation, curvature, curvature_prime, path_length, width

    @staticmethod
    def _offset_path(
        path: "Iterable[np.ndarray]", offset: np.ndarray
    ) -> "Iterable[np.ndarray]":
        for pos in path:
            yield pos + offset

    @staticmethod
    def _extend_polyline(polyline: np.ndarray, extend_length: float):
        diff_last = polyline[-1] - polyline[-2]
        norm_diff = np.linalg.norm(diff_last)
        extended_node = polyline[-1] + diff_last * extend_length / norm_diff
        return np.concatenate((polyline, extended_node.reshape(1, -1)), axis=0)

    @classmethod
    def _create_curvilinear_coordinate_system_from_reference(
        cls, ref_path: np.ndarray
    ) -> CurvilinearCoordinateSystem:
        """
        Generates curvilinear coordinate system for a reference path

        :param ref_path: reference path (polyline)
        :returns curvilinear coordinate system for reference path
        """
        new_ref_path = ref_path
        for i in range(0, Cfg["common"]["lane"].get("num_chankins_corner_cutting")):
            new_ref_path = chaikins_corner_cutting(new_ref_path)
        new_ref_path = cls._extend_polyline(
            new_ref_path, Cfg["common"]["lane"].get("extend_length")
        )
        new_ref_path = resample_polyline(
            new_ref_path, Cfg["common"]["lane"].get("polyline_resampling_step")
        )

        curvilinear_cosy = CurvilinearCoordinateSystem(
            new_ref_path,
            Cfg["common"]["lane"].get("lateral_projection_domain_limit"),
            Cfg["common"]["lane"].get("lateral_eps"),
        )

        return curvilinear_cosy

    @staticmethod
    def _compute_width_from_lanalet_boundary(
        left_polyline: np.ndarray, right_polyline: np.ndarray
    ) -> np.ndarray:
        """
        Computes the width of a lanelet

        :param left_polyline: left boundary of lanelet
        :param right_polyline: right boundary of lanelet
        :return: width along lanelet
        """
        width_along_lanelet = np.zeros((len(left_polyline),))
        for i in range(len(left_polyline)):
            width_along_lanelet[i] = np.linalg.norm(
                left_polyline[i] - right_polyline[i]
            )
        return width_along_lanelet

    def __init__(
        self,
        id: int,
        clcs: CurvilinearCoordinateSystem,
        clcs_left: CurvilinearCoordinateSystem,
        clcs_right: CurvilinearCoordinateSystem,
        orientation: np.ndarray,
        curvature: np.ndarray,
        curvature_prime: np.ndarray,
        path_length: np.ndarray,
        width: np.ndarray,
        segment_ids: "Optional[List[int]]" = None,
        adj_left: "Optional[int]" = None,
        adj_right: "Optional[int]" = None,
        is_real: bool = True,
    ) -> None:
        self.id = id
        self.clcs_left = clcs_left
        self.clcs_right = clcs_right
        self.clcs = clcs
        self._orientation = orientation
        self._curvature = curvature
        self._curvature_prime = curvature_prime
        self._path_length = path_length
        self._width = width
        self.is_real = is_real

        if segment_ids is None:
            segment_ids = [id]
        self.segment_ids = segment_ids
        self.adj_left = adj_left
        self.adj_right = adj_right

    def orientation(
        self, position: "Union[float, np.ndarray]"
    ) -> "Union[float, np.ndarray]":
        """
        Calculates orientation of lane given a longitudinal position along lane

        :param position: longitudinal position
        :returns orientation of lane at a given position
        """
        return np.interp(position, self._path_length, self._orientation)

    def width(self, position: "Union[float, np.ndarray]") -> "Union[float, np.ndarray]":
        """
        Calculates width of lane given a longitudinal position along lane

        :param position: longitudinal position
        :returns width of lane at a given position
        """
        return np.interp(position, self._path_length, self._width)

    def curvature(
        self, position: "Union[float, np.ndarray]"
    ) -> "Union[float, np.ndarray]":
        return np.interp(position, self._path_length, self._curvature)

    def curvature_prime(
        self, position: "Union[float, np.ndarray]"
    ) -> "Union[float, np.ndarray]":
        return np.interp(position, self._path_length, self._curvature_prime)

    def contains_point(self, x: float, y: float) -> bool:
        return self.contains_point_left(x, y) and self.contains_point_right(x, y)


class RoadNetwork(AbstractStructure):
    @classmethod
    def create_from_scenario(
        cls,
        scenario: "Scenario",
    ) -> "RoadNetwork":
        lanelet_network = scenario.lanelet_network

        # for simple (highway) scenarios
        lanes = {
            lanelet.lanelet_id: Lane.create_from_lanelet(lanelet.lanelet_id, lanelet)
            for lanelet in lanelet_network.lanelets
        }

        ## for complex scenarios
        # start_lanelets = []
        # for lanelet in lanelet_network.lanelets:
        #     if len(lanelet.predecessor) == 0:
        #         start_lanelets.append(lanelet)
        #     else:
        #         predecessors = [
        #             lanelet_network.find_lanelet_by_id(pred_id)
        #             for pred_id in lanelet.predecessor
        #         ]
        #         for pred in predecessors:
        #             if not lanelet.lanelet_type == pred.lanelet_type:
        #                 start_lanelets.append(lanelet)

        # lane_lanelets = []
        # for lanelet in start_lanelets:
        #     (
        #         merged_lanelets,
        #         merge_jobs,
        #     ) = Lanelet.all_lanelets_by_merging_successors_from_lanelet(
        #         lanelet,
        #         lanelet_network,
        #         road_network_param.get("merging_length"),
        #     )
        #     if len(merged_lanelets) == 0 or len(merge_jobs) == 0:
        #         merged_lanelets.append(lanelet)
        #         merge_jobs.append([lanelet.lanelet_id])
        #     for idx in range(len(merged_lanelets)):
        #         lane_lanelets.append((merged_lanelets[idx], merge_jobs[idx]))

        # lanes = {}
        # for lanelet, merge_jobs in lane_lanelets:
        #     if len(merge_jobs) == 1:
        #         id = merge_jobs[0]
        #     else:
        #         id = scenario.generate_object_id()
        #     lanes[id] = Lane.create_from_lanelet(id, lanelet, road_network_param)
        if Cfg["common"]["road_network"]["use_phantom_lane"]:
            phantom_lanes = cls._create_phantom_lane(lanes, scenario)
            lanes.update(phantom_lanes)
        return cls(lanes)

    @staticmethod
    def _create_phantom_lane(
        lanes: "Dict[int, Lane]", scenario: "Scenario"
    ) -> "Dict[int, Lane]":
        left_lane = None
        right_lane = None
        for lane in lanes.values():
            if lane.adj_left is None:
                left_lane = lane
            if lane.adj_right is None:
                right_lane = lane
            if left_lane is not None and right_lane is not None:
                break

        phantom_left, phantom_right = Lane.create_phantom_from_lanes(
            left_lane, right_lane, scenario
        )
        left_lane.adj_left = phantom_left.id
        right_lane.adj_right = phantom_right.id

        return {phantom_left.id: phantom_left, phantom_right.id: phantom_right}

    def __init__(self, lanes: "Dict[int, Lane]") -> None:
        self._lanes = lanes
        self._assign_parent()

    @property
    def lanes(self) -> "List[Lane]":
        return list(self._lanes.values())

    @property
    def lane_ids(self) -> "List[int]":
        return list(self._lanes.keys())

    def has_lane(self, id: int) -> bool:
        return id in self._lanes

    @property
    def real_lanes(self) -> "List[Lane]":
        return list(l for l in self._lanes.values() if l.is_real)

    @property
    def real_lane_ids(self) -> "List[int]":
        return list(id for id, l in self._lanes.items() if l.is_real)

    @property
    def phantom_lanes(self) -> "List[Lane]":
        return list(l for l in self._lanes.values() if not l.is_real)

    @property
    def phantom_lane_ids(self) -> "List[int]":
        return list(id for id, l in self._lanes.items() if not l.is_real)

    @property
    def left_most_real_lane(self) -> "Lane":
        return self.lane_by_id(self.left_most_real_lane_id)

    @property
    def left_most_real_lane_id(self) -> int:
        if not hasattr(self, "_left_most_real_lane_id"):
            self._left_most_real_lane_id = self.left_most_lane(self.real_lanes)[0].id
        return self._left_most_real_lane_id

    @property
    def right_most_real_lane(self) -> "Lane":
        return self.lane_by_id(self.right_most_real_lane_id)

    @property
    def right_most_real_lane_id(self) -> int:
        if not hasattr(self, "_right_most_real_lane_id"):
            self._right_most_real_lane_id = self.right_most_lane(self.real_lanes)[0].id
        return self._right_most_real_lane_id

    def lane_by_id(self, id: int) -> "Lane":
        if id in self._lanes:
            return self._lanes[id]

    def lane_by_position(self, x: float, y: float) -> "Optional[Lane]":
        for lane in self.lanes:
            if lane.contains_point(x, y):
                return lane
        return None

    def lane_by_vehicle_state(self, state: "VehicleState") -> "Optional[Lane]":
        state = state.get_state_in_world_frame()
        return self.lane_by_position(state.s, state.d)

    def lanes_by_positions(self, positions: np.ndarray) -> "List[Lane]":
        return [self.lane_by_position(x, y) for x, y in positions]

    def lanes_by_shape(self, shape: "Shape") -> "List[Lane]":
        world_state: "World" = self.parent()
        lanelet_ids = set(
            world_state.scenario.lanelet_network.find_lanelet_by_shape(shape)
        )
        lanes = [lane for lane in self.lanes if lanelet_ids & set(lane.segment_ids)]
        return lanes

    def lanes_by_vehicle_state(self, state: "VehicleState") -> "List[Lane]":
        state = state.get_state_in_world_frame()
        world_state: "World" = self.parent()
        vehicle_id = state.get_parent_recursively_as_type("Vehicle").id
        shape = world_state.scenario.obstacle_by_id(
            vehicle_id
        ).obstacle_shape.rotate_translate_local(
            np.array([state.s, state.d]), state.theta
        )
        return self.lanes_by_shape(shape)

    def left_most_lane(self, lanes: "List[Lane]") -> "List[Lane]":
        if len(lanes) == 0:
            return None
        if len(lanes) == 1:
            return lanes

        lanelet_ids = set(id for lane in lanes for id in lane.segment_ids)
        return [l for l in lanes if l.adj_left not in lanelet_ids]

    def right_most_lane(self, lanes: "List[Lane]") -> "List[Lane]":
        if len(lanes) == 0:
            return None
        if len(lanes) == 1:
            return lanes

        lanelet_ids = set(id for lane in lanes for id in lane.segment_ids)
        return [l for l in lanes if l.adj_right not in lanelet_ids]

    def _assign_parent(self) -> None:
        for lane in self.lanes:
            lane.set_parent(self)
