import os
from typing import TYPE_CHECKING

from commonroad.common.file_reader import CommonRoadFileReader
from commonroad.scenario.obstacle import ObstacleType
from commonroad_mpr.utils.configuration_builder import ConfigurationBuilder as Cfg
from commonroad_mpr.utils.visualization import plot_vehicle, visualize_static_scenario

from .road_network import RoadNetwork
from .vehicle import AbstractStructure, Vehicle

if TYPE_CHECKING:
    from typing import Dict, Iterable, List, Optional, Tuple

    from commonroad.scenario.scenario import Scenario
    from matplotlib.axes import Axes
    from matplotlib.figure import Figure

    from .vehicle import VehicleState


class World(AbstractStructure):
    @classmethod
    def create_from_scenario(cls, scenario: "Scenario") -> "World":
        """
        Create world from the given scenario.
        :param scenario: CommonRoad scenario
        :return: World instance
        """
        road_network = RoadNetwork.create_from_scenario(scenario)
        vehicles = {
            obs.obstacle_id: Vehicle.create_from_dynamic_obstacle(obs)
            for obs in scenario.dynamic_obstacles
            if (
                (not Cfg["common"]["world_state"]["only_cars"])
                or obs.obstacle_type == ObstacleType.CAR
            )
            and obs.prediction is not None
        }
        world_state = cls(road_network, vehicles, scenario)
        for vehicle in world_state.vehicles:
            vehicle.trajectory_persp.trajectories[0].compute_missing_variables()
        return world_state

    def __init__(
        self,
        road_network: "RoadNetwork",
        vehicles: "Dict[int, Vehicle]",
        scenario: "Scenario",
    ) -> None:
        scenario.assign_obstacles_to_lanelets()
        self.scenario = scenario
        self.road_network = road_network
        self._vehicles = vehicles
        self._assign_parent()

    @property
    def vehicles(self) -> "List[Vehicle]":
        return list(self._vehicles.values())

    @property
    def vehicle_ids(self) -> "List[int]":
        return list(self._vehicles.keys())

    @property
    def dt(self) -> float:
        return self.scenario.dt

    def vehicle_by_id(self, v_id: int) -> Vehicle:
        return self._vehicles[v_id]

    def has_vehicle(self, v_id: int) -> bool:
        return v_id in self._vehicles

    def _assign_parent(self) -> None:
        for vehicle in self.vehicles:
            vehicle.set_parent(self)
        self.road_network.set_parent(self)

    def plot(
        self,
        start_time_step: "Optional[int]" = None,
        end_time_step: "Optional[int]" = None,
        title: "Optional[str]" = None,
        plot_limits: "Optional[List[float]]" = None,
        scale_factor: "Optional[int]" = 1,
        vehicle_ids: "Optional[List[int]]" = None,
        ego_ids: "List[int]" = None,
    ) -> "Tuple[Figure, Axes]":
        if ego_ids is None:
            ego_ids = []
        if start_time_step is None:
            start_time_step = 0
        if vehicle_ids is None:
            vehicle_ids = self.vehicle_ids

        fig, ax = visualize_static_scenario(
            self.scenario,
            plot_limits,
            scale_factor,
        )

        for v_id in ego_ids:
            plot_vehicle(
                ax,
                self.scenario.obstacle_by_id(v_id),
                start_time_step,
                None,
                plot_limits,
                str(v_id),
                "yellow",
            )
        for v_id in vehicle_ids:
            plot_vehicle(
                ax,
                self.scenario.obstacle_by_id(v_id),
                start_time_step,
                end_time_step,
                plot_limits,
                str(v_id),
                "blue"
            )

        if title is not None:
            ax.title.set_text(title)

        return fig, ax


class ScenarioReader:
    @classmethod
    def read_scenario(cls, scenario_id: str) -> "World":
        scenario_path = os.path.join(
            Cfg["path"]["path_scenarios"], scenario_id + ".xml"
        )
        scenario, _ = CommonRoadFileReader(scenario_path).open()
        return World.create_from_scenario(scenario)

    @classmethod
    def world_state_iter(cls) -> "Iterable[World]":
        file_names = os.listdir(Cfg["path"]["path_scenarios"])
        for file_name in file_names:
            root, ext = os.path.splitext(file_name)
            if ext == ".xml":
                yield cls.read_scenario(root)


class ParseError(Exception):
    pass


def parse(**kwargs) -> "Tuple[World, List[Vehicle], List[VehicleState]]":
    """parse inputs for predicate evaluation, robustness evaluation, and feature variable evaluation.

    Args:
        world_state (WroldState):
        vehicles (List[Vehicle]): [ego_vehicle, other_vehicle]. If world_state is missing, generate from it
        time_step (int):
        scenario (Scenario): If world_state is missing, generate from it
        vehicle_ids (List[int]): [ego_id, other_id]. If vehicles is missing, get vehicles from it.
        states (List[VehicleState]): [ego_state, other_state]. If world_state, vehicles, or time_step
            are missing, get them from it.
    Returns:
        Tuple[World, List[Vehicle], List[VehicleState]]: world_state, vehicles, states
    """

    if "world_state" in kwargs:
        world_state = kwargs["world_state"]
    elif "vehicles" in kwargs:
        world_state = kwargs["vehicles"][0].parent()
    elif "states" in kwargs:
        world_state = kwargs["states"][0].get_parent_recursively_as_type("World")
    elif "scenario" in kwargs:
        world_state = World.create_from_scenario(kwargs["scenario"])
    else:
        raise ParseError("cannot parse world_state from given input.")

    if "vehicles" in kwargs:
        vehicles = kwargs["vehicles"]
    elif "states" in kwargs:
        vehicles = [
            state.get_parent_recursively_as_type("Vehicle")
            for state in kwargs["states"]
        ]
    elif "vehicle_ids" in kwargs:
        vehicles = [world_state.vehicle_by_id(id) for id in kwargs["vehicle_ids"]]
    else:
        raise ParseError("cannot parse vehicles from given input.")

    if "states" in kwargs:
        states = kwargs["states"]
    elif "time_step" in kwargs:
        ref_lane = vehicles[0].trajectory_persp.get_reference_lane(kwargs["time_step"])
        states = [
            vehicle.trajectory_persp.trajectory_by_clcs_id(
                ref_lane.id
            ).state_at_time_step(kwargs["time_step"])
            for vehicle in vehicles
        ]
    else:
        raise ParseError("cannot parse states from given input.")

    return world_state, vehicles, states
